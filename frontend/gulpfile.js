var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    return gulp.src('scss/**/styles.scss')
        .pipe(sass())
        .pipe(gulp.dest('../public/css'));
});

//var watcher = gulp.watch('scss/**/*.scss');
//watcher.on('all', function(event, path, stats) {
//    gulp.series('styles');
//});

gulp.task('watch', function() {
    gulp.watch(['scss/**/*.scss'], ['styles']);
});

//gulp.task('watch:styles', function() {
//    gulp.watch('scss/**/*.scss', gulp.series('styles'));
//});

//gulp.task('watch', gulp.series('styles', gulp.parallel('watch:styles')));
//gulp.task('default', gulp.series('styles', gulp.parallel('watch')));

//gulp.watch('scss/**/*.scss', gulp.parallel('styles'));
gulp.task('default', ['watch']);
gulp.task('ci', ['styles']);