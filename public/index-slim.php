<?php

declare(strict_types = 1);

use App\Application\Forms\Search\SearchForm;
use App\Application\Services\FriendsService;
use App\Application\Services\Search\RequestParser\TagsFactory;
use App\Application\Services\UserSearchService;
use App\Application\Services\MessageService;
use App\Application\Services\ProfileService;
use App\Application\Services\SerializeService;
use App\Domain\Model\User\UserId;
use App\System\Authentication\WebAuthentication;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use Slim\Http\Request as Req;
use Slim\Http\Response as Res;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

require '../vendor/autoload.php';

$config                           = [];
$config['displayErrorDetails']    = true;
$config['addContentLengthHeader'] = false;

$app       = new \Slim\App(['settings' => $config]);
$container = $app->getContainer();
$router    = $container->get('router');

$container['view'] = function(Container $container) {
    $view = new \Slim\Views\Twig('../src/UI/Web/views', [
        //        'cache' => 'path/to/cache'
    ]);

    $router = $container->get('router');
    $uri    = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};

/** Routes that need an authentication */
$app->group(null, function() {
    $this->get('/messages', function($req, $res) {
        return $this->view->render($res, 'messages/list.twig', [
            'messages' => (new MessageService())->getList()
        ]);
    })->setName('messages');

    $this->get('/friends', function($req, $res) {
        $friendsService = new FriendsService();
        $userId         = $_SESSION['user']->id();

        return $this->view->render($res, 'friends/list.twig', [
            'friends' => $friendsService->getUserFriends((string) $userId)
        ]);
    })->setName('friends');

    $this->get('/friends/suggest/{friendId}', function($req, $res, $arg) {
        $friendsService = new UserSearchService();
        $userId         = $_SESSION['user']->id();
        $friendId       = $arg['friendId'];

        return $this->view->render($res, 'friends/list.twig', [
            'friends' => $friendsService->getUserFriends((string) $userId)
        ]);
    })->setName('friends');

    $this->get('/messages/show/{id}', function($req, $res, $arg) {
        $message = (new MessageService())->get($arg['id']);

        return $this->view->render($res, 'messages/show.twig', ['message' => $message]);
    })->setName('messages.show');

    $this->get('/messages/conversation/{userId}', function(Req $req, $res, $arg) {
        $conversation = (new MessageService())->conversation(
            $_SESSION['user']->id(),
            new UserId($arg['userId'])
        );

        return $this->view->render($res, 'messages/conversation.twig', ['conversation' => $conversation]);
    })->setName('messages.conversation.show');

    $this->post('/messages/conversation/{userId}', function(Req $req, Res $res, $arg) {
        (new MessageService())->write(
            (string) $_SESSION['user']->id(),
            $arg['userId'],
            $req->getParam('message')
        );
    })->setName('messages.conversation.show');

    $this->get('/search', function(Req $req, Res $res) {
        //        $searchUsers = new App\Application\SearchUsers();
        //        $users       = $searchUsers->getDto()->getUsers();

        $params        = $req->getParams();
        $searchService = new UserSearchService();
        $criteria      = $searchService->factory($params);
        //        var_dump($criteria);
        $tags = TagsFactory::stringTags($criteria);
        //        var_dump($tags);
        //        //        var_dump($params);
        //        //        var_dump($criteria);
        //        exit;
        $users = $searchService->search($tags);
        //        var_dump($users);
        //        exit;

        return $this->view->render($res, 'user/search_result.twig', ['users' => $users]);
    })->setName('search.get');

    $this->get('/panel', function($req, $res) {
        return $this->view->render($res, 'panel/index.twig');
    })->setName('panel');

    $this->post('/locations', function($req, Res $res) {
        $locations = [
            ['city' => 'Warsaw', 'country' => 'Poland'],
            ['city' => 'Wrocław', 'country' => 'Poland']
        ];

        return $res->withJson($locations, 200);
    })->setName('locations');

    $this->get('/logout', function($req, Res $res) {
        (new WebAuthentication())->logout();

        return $res->withRedirect('/');
    })->setName('auth.logout');

    $this->get('/photo/{id}', function($req, Res $res, array $args) {
        $filename = $args['id'];
        $filePath = '../resources/photos/' . $filename;

        if ( ! file_exists($filePath)) {
            throw new \InvalidArgumentException("No file $filename");
        }

        $handle = fopen($filePath, 'rb');
        $file   = fread($handle, filesize($filePath));
        fclose($handle);

        header('Content-Type: image/jpeg');
        echo $file;
        exit;
    })->setName('photo');

    $this->post('/photo', function(Req $req, Res $res, array $args) {
    })->setName('photo.upload');

    $this->get('/profile/{id}', function(Req $req, Res $res, array $args) {
        $profile    = (new ProfileService())->get($args['id']);
        $properties = str_replace(['{', '}'], '', $profile->properties());
        $properties = explode(',', $properties);

        foreach ($properties as $tag) {
            [$property, $value] = explode('&', substr_replace(
                $tag, '&', strrpos($tag, '_'), strlen('&')
            ));

            $result[$property] = $value;
        }

        ksort($result);

        //        var_dump($result);
        //        exit;

        return $this->view->render($res, 'profile/show.twig', [
            'profile'    => $profile,
            'properties' => $result
        ]);
    })->setName('profile');
})
    ->add(function($req, Res $res, $next) use ($router) {
        $auth = new WebAuthentication();

        if ($auth->isGuest()) {
            return $res->withRedirect('/login');
        }

        /** Add user's info to every route
         *
         * @var Twig               $view
         * @var ContainerInterface $this
         */
        $view = $this->get('view');
        $view->getEnvironment()->addGlobal('logged_user', $_SESSION['user']);

        $formHelper       = new \Zend\Form\View\Helper\Form();
        $rowHelper        = new \Zend\Form\View\Helper\FormRow();
        $collectionHelper = new \Zend\Form\View\Helper\FormCollection();
        $inputHelper      = new \Zend\Form\View\Helper\FormInput();
        $elementHelper    = new \Zend\Form\View\Helper\FormElement();
        $labelHelper      = new \Zend\Form\View\Helper\FormLabel();
        $renderer         = new \Zend\View\Renderer\PhpRenderer();
        $configProvider   = new \Zend\Form\ConfigProvider();
        $errorHelper      = new \Zend\Form\View\Helper\FormElementErrors();

        $renderer->setHelperPluginManager(
            new \Zend\View\HelperPluginManager(
                new \Zend\ServiceManager\ServiceManager(),
                $configProvider()['view_helpers']
            )
        );

        $elementHelper->setView($renderer);
        $formHelper->setView($renderer);
        $rowHelper->setView($renderer);
        $labelHelper->setView($renderer);
        //        $elementHelper->setView($renderer);

        $form = new SearchForm('wyszukiwarka');
        $form->prepare();

        $fieldsets = $form->getFieldsets();
        //
        //        foreach ($fieldsets['fieldsets'] as $fieldset) {
        //            //            var_dump($rowHelper($fieldset, 'append'));
        //            //            var_dump($labelHelper($fieldset));
        //            //            var_dump($elementHelper($fieldset));
        //        }
        //        exit;
        //
        //        var_dump($fieldsets['fieldsets']);
        //        exit;
        //
        //        $str   = [];
        //        $str[] = $formHelper->openTag($form);
        //        $str[] = $labelHelper($form->get('city'));
        //        $str[] = $formHelper->render($form);
        //        //        var_dump($formHelper->getView());
        //        //        exit;
        //        //        $str[] = $inputHelper($form->get('city'));
        //        //        $str[] = $rowHelper($form->get('city'));
        //        //        $str[] = $elementHelper($form->get('submit'));
        //        $str[] = $formHelper->closeTag();
        //
        //        $formHtml = join('', $str);
        //
        //        //        var_dump($formHtml);
        //        //        var_dump($str);
        //        //        exit;
        //
        //        $view->getEnvironment()->addGlobal('search_form', $formHtml);

        return $next($req, $res);
    });

/** Public routes */
$app->group(null, function() {
    $this->get('/', function($req, $res) {
        return $this->view->render($res, 'main/index.twig');
    })->setName('main.get');

    $this->post('/registration', function(Req $req, Res $res) {
        return $res->withRedirect('/registration/confirmation');
        //        return $this->view->render($res, 'user/registration.twig');
    })->setName('registration');

    $this->get('/registration/confirmation', function(Req $req, Res $res) {
        //        $auth = new WebAuthentication();
        //
        //        if ($auth->isLogged()) {
        //            return $res->withRedirect('/messages');
        //        }

        return $this->view->render($res, 'user/registration.twig');
    })->setName('registration.confirmation');

    $this->get('/registration', function(Req $req, Res $res) {
        //        $auth = new WebAuthentication();
        //
        //        if ($auth->isLogged()) {
        //            return $res->withRedirect('/messages');
        //        }

        return $this->view->render($res, 'user/registration.twig');
    })->setName('registration');

    $this->get('/login', function($req, Res $res) {
        //        $auth = new WebAuthentication();
        //
        //        if ($auth->isLogged()) {
        //            return $res->withRedirect('/messages');
        //        }

        return $this->view->render($res, 'user/login.twig');
    })->setName('login.get');

    $this->post('/login', function(Req $req, Res $res) {
        (new WebAuthentication())->login(
            $req->getParam('login'),
            $req->getParam('password')
        );

        return $res->withRedirect('/messages');
    })->setName('login.post');
})
    ->add(function($req, Res $res, $next) use ($router) {
        if ((new WebAuthentication())->isLogged()) {
            return $res->withRedirect('/messages');
        }

        return $next($req, $res);
    });

$app->run();
