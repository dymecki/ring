<?php

require_once '../vendor/autoload.php';

use App\UI\Web\Render;
use App\Authentication\WebAuthentication;

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['logged'])) {
    if ($_SESSION['logged']) {
        header('Location: admin.php');
        exit;
    }
}

if (isset($_POST['login']) && isset($_POST['password'])) {
    (new WebAuthentication())->login($_POST['login'], $_POST['password']);
}

if (isset($_SESSION['logged'])) {
    if ($_SESSION['logged']) {
        header('Location: admin.php');
        exit;
    }
}

echo (new Render())->get('login.twig');
