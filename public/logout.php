<?php

require_once '../vendor/autoload.php';

use App\Authentication\WebAuthentication;

(new WebAuthentication())->logout();

header('Location: admin.php');
exit;
