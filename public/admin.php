<?php

require_once '../vendor/autoload.php';

use App\UI\Web\Render;
use App\Authentication\WebAuthentication;

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if (!(new WebAuthentication())->isLogged()) {
    header('Location: login.php');
    exit;
}

echo '<h3>Admin panel</h3>';
echo '<a href="/logout.php">Log out</a>';

//var_dump($_SESSION);



//echo (new Render())->get('login.twig');
