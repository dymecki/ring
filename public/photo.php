<?php

$filename = $_GET['id'];
$filePath = '../resources/photos/' . $filename;

if (!file_exists($filePath)) {
    throw new \InvalidArgumentException("No file $filename");
}

$handle = fopen($filePath, 'r');
$file   = fread($handle, filesize($filePath));
fclose($handle);

header('Content-Type: image/jpeg');
echo $file;
exit;
