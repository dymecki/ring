#!/usr/bin/env php
<?php
require_once __DIR__ . '/inc/bootstrap.php';

use Symfony\Component\Console\Application;
use App\Admin\RegisterUserConsoleCommand;

$app = new Application('Ring Admin', '0.1.0');
$app->add(new RegisterUserConsoleCommand());
$app->run();
