<?php


use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesLanguage extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.languages')
            ->addColumn('name', 'string')
            ->addTimestamps()
            ->create();
    }
}
