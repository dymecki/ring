<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;

class CreateTableFriends extends AbstractMigration
{
    public function change(): void
    {
        $this->table('users.friends', ['id' => false])
             ->addColumn('users_hash', 'string')
             ->addColumn('user_id', 'uuid')
             ->addColumn('friend_id', 'uuid')
             ->addColumn('status', 'integer')
             ->addTimestamps()
             ->addIndex(['user_id', 'friend_id'], ['unique' => true])
             ->addIndex(['user_id'])
             ->addIndex(['friend_id'])
             ->create();
    }
}
