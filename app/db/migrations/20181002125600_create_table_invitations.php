<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTableInvitations extends AbstractMigration
{
    public function change()
    {
        $this->table('users.invitations', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
            ->addColumn('inviter_id', 'uuid')
            ->addTimestamps()
            ->create();
    }
}
