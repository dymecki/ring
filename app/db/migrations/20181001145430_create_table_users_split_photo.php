<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTableUsersSplitPhoto extends AbstractMigration
{
    public function change()
    {
        $this->table('users.split_photos', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
            ->addColumn('photo_a', 'uuid')
            ->addColumn('photo_b', 'uuid')
            ->addTimestamps()
            ->create();
    }
}
