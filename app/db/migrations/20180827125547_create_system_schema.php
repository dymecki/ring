<?php

use Phinx\Migration\AbstractMigration;

class CreateSystemSchema extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA system');
    }

}
