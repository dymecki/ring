<?php


use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesHairColor extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.hair_color')
            ->addColumn('name', 'string')
            ->addTimestamps()
            ->create();
    }
}
