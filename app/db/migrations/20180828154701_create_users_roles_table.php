<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersRolesTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.roles')
            ->addColumn('name', 'string')
            ->addTimestamps()
            ->addIndex('id')
            ->create();
    }

}
