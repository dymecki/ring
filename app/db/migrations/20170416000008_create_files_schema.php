<?php

use Phinx\Migration\AbstractMigration;

class CreateFilesSchema extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA files');
    }

}
