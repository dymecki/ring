<?php


use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesGender extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.gender')
            ->addColumn('name', 'string')
            ->addTimestamps()
            ->create();
    }
}
