<?php


use Phinx\Migration\AbstractMigration;

class CreateTableSystemSessions extends AbstractMigration
{
    public function change()
    {
        $this->table('system.sessions', ['id' => false, 'primary_key' => ['id', 'name']])
            ->addColumn('id', 'uuid')
            ->addColumn('name', 'text')
            ->addColumn('modified', 'integer')
            ->addColumn('lifetime', 'integer')
            ->addColumn('data', 'text')
            ->addTimestamps()
            ->create();
    }
}
