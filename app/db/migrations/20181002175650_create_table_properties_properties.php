<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTablePropertiesProperties extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.properties')
            ->addColumn('name', 'text')
            ->addColumn('values', Literal::from('text[]'))
            ->addTimestamps()
            ->create();
    }
}
