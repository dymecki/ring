<?php


use Phinx\Migration\AbstractMigration;

class CreateUsersMembershipsTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.memberships')
            ->addColumn('user_id', 'uuid')
            ->addTimestamps()
            ->create();
    }
}
