<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTableProfilesPhotos extends AbstractMigration
{
    public function change()
    {
        $this->table('users.photos', ['id' => false, 'primary_key' => 'id'])
             ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
             ->addColumn('user_id', 'uuid')
             ->addColumn('photo_id', 'uuid')
             ->addTimestamps()
             ->create();
    }
}
