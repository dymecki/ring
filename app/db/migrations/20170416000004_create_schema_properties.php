<?php

use Phinx\Migration\AbstractMigration;

class CreateSchemaProperties extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA IF NOT EXISTS properties');
    }

}
