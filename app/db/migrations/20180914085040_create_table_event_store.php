<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTableEventStore extends AbstractMigration
{
    public function change(): void
    {
        $this->table('system.event_store', ['id' => false, 'primary_key' => 'event_id'])
             ->addColumn('event_id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
             ->addColumn('aggregate_id', 'uuid')
             ->addColumn('event_type', 'text')
             ->addColumn('data', 'jsonb')
             ->addColumn('version', 'integer')
             ->addTimestamps()
             ->create();
    }
}
