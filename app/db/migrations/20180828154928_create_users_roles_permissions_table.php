<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersRolesPermissionsTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.roles_permissions')
            ->addColumn('role_id', 'integer')
            ->addColumn('permission_id', 'integer')
            ->addTimestamps()
            ->create();
    }

}
