<?php


use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesEyeColor extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.eye_color')
            ->addColumn('name', 'string')
            ->addTimestamps()
            ->create();
    }
}
