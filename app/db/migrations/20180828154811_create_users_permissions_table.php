<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersPermissionsTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.permissions')
            ->addColumn('description', 'string')
            ->addTimestamps()
            ->addIndex('id')
            ->create();
    }

}
