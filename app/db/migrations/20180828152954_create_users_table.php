<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateUsersTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.users', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
            ->addColumn('name', 'string')
            ->addColumn('email', 'string')
            ->addColumn('password_hash', 'string')
            ->addTimestamps()
            ->create();
    }

}
