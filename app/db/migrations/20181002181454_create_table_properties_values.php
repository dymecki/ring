<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesValues extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.values')
             ->addColumn('property_id', 'integer')
             ->addColumn('value', 'text')
             ->addTimestamps()
             ->create();
    }
}
