<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersUsersRolesTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.users_roles')
            ->addColumn('user_id', 'uuid')
            ->addColumn('role_id', 'integer')
            ->addTimestamps()
            ->create();
    }

}
