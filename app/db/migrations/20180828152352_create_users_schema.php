<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersSchema extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA users');
    }

}
