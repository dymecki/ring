<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateProfilesTable extends AbstractMigration
{
    public function change(): void
    {
        $this->table('users.profiles', ['id' => false, 'primary_key' => 'id'])
             ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
             ->addColumn('user_id', 'uuid')
             ->addColumn('name', 'text')
             ->addColumn('birthdate', 'date')
             ->addColumn('properties', 'text[]')
             ->addTimestamps()
             ->create();
    }

}
