<?php

use Phinx\Migration\AbstractMigration;

class CreateSystemLogsTable extends AbstractMigration
{
    public function change()
    {
        $this->table('system.logs', ['id' => false])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('channel', 'string')
            ->addColumn('message', 'text')
            ->addColumn('level_name', 'string', ['null' => true])
            ->addColumn('context', 'text')
            ->addColumn('user_id', 'uuid')
            ->addColumn('ip', 'inet', ['null' => true])
            ->addColumn('process_id', 'integer')
            ->addColumn('memory_usage', 'string')
            ->addColumn('extra', 'text', ['null' => true])
            ->addColumn('request_headers', 'text', ['null' => true])
            ->addIndex('channel')
            ->addIndex('level_name')
            ->addIndex('user_id')
            ->create();
    }

}
