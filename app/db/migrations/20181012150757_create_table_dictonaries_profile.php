<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateTableDictonariesProfile extends AbstractMigration
{
    public function change()
    {
        $this->table('dictionaries.profile')
             ->addColumn('properties', Literal::from('jsonb'))
             ->addTimestamps()
             ->create();
    }
}
