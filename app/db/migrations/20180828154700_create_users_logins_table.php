<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateUsersLoginsTable extends AbstractMigration
{
    public function change()
    {
        $this->table('users.logins', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
            ->addColumn('user_id', 'uuid')
            ->addColumn('name', 'string')
            ->addColumn('password', 'string')
            ->addTimestamps()
            ->create();
    }

}
