<?php


use Phinx\Migration\AbstractMigration;

class CreateTablePropertiesHeight extends AbstractMigration
{
    public function change()
    {
        $this->table('properties.height')
            ->addColumn('height', 'string')
            ->addTimestamps()
            ->create();
    }
}
