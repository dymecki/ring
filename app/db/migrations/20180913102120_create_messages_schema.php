<?php


use Phinx\Migration\AbstractMigration;

class CreateMessagesSchema extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA IF NOT EXISTS messages');
    }
}
