<?php


use Phinx\Migration\AbstractMigration;

class CreateSchemaProfiles extends AbstractMigration
{
    public function change()
    {
        $this->execute('CREATE SCHEMA IF NOT EXISTS profiles');
    }
}
