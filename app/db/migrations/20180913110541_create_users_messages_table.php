<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateUsersMessagesTable extends AbstractMigration
{
    public function change()
    {
        $this->table('messages.messages', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'uuid', ['default' => Literal::from('uuid_generate_v4()')])
            ->addColumn('sender_id', 'uuid')
            ->addColumn('receiver_id', 'uuid')
            ->addColumn('read', 'boolean', ['default' => false])
            ->addColumn('message', 'text')
            ->addTimestamps()
            ->addForeignKey('sender_id', 'users.users', 'id')
            ->addForeignKey('receiver_id', 'users.users', 'id')
            ->create();
    }
}
