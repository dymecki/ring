<?php

declare(strict_types = 1);

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
                            'driver'    => 'pgsql',
                            'host'      => 'localhost',
                            'database'  => 'ring',
                            'username'  => 'ring',
                            'password'  => 'secret',
                            'charset'   => 'utf8',
                            'collation' => 'utf8_unicode_ci',
                            'prefix'    => ''
                        ]);

$capsule->setAsGlobal();
$capsule->bootEloquent();