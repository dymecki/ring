<?php

declare(strict_types = 1);

include __DIR__ . '/../../../vendor/autoload.php';

use Phinx\Seed\AbstractSeed;

class DictionaryProfileSeeder extends AbstractSeed
{
    public function run()
    {
        $profileProperties = [
            'gender' => ['Woman', 'Man'],
            'hair_color' => [
                'Blonde',
                'Brown',
                'Black',
                'Silver',
                'Bald'
            ],
            'eye_color' => [
                'Brown',
                'Blue',
                'Green',
                'Black'
            ],
            'zodiac' => [
                'Aquarius'
            ],
            'language' => [
                'english',
                'german',
                'french',
                'spanish'
            ],
            'nationality' => [
                'Italian',
                'German',
                'French',
                'American'
            ],
            'religion' => [
                'Catholic',
                'Protestant',
                'Muslim',
                'Buddism',
                'Atheism',
                'Judaism',
                'Hinduism'
            ],
            'alcohol' => ['no', 'yes', 'sometimes'],
            'tattoo' => [
                'No',
                'Yes',
                'No, but I want',
                'Yes, but I want to remove it'
            ]
        ];

        $json = json_encode($profileProperties, JSON_PRETTY_PRINT);

        $data = ['properties' => $json];

        $this->table('dictionaries.profile')->insert($data)->save();

    }

}
