<?php

declare(strict_types = 1);

include __DIR__ . '/../../../vendor/autoload.php';

use Phinx\Seed\AbstractSeed;

class MessageSeeder extends AbstractSeed
{
    private const MESSAGES_PER_USER = 10;

    private $users;
    private $messages = [];
    /** @var \Faker\Generator */
    private $faker;

    public function run(): void
    {
        $this->faker = Faker\Factory::create();
        $this->users = array_column($this->fetchAll('SELECT id FROM users.users'), 'id');

        for ($i = 0, $iMax = count($this->users); $i < $iMax; $i++) {
            $this->addMessages($this->getAuthors());
        }

        $this->table('messages.messages')->insert($this->messages)->save();
    }

    private function addMessages(object $authors): void
    {
        for ($j = 0; $j < $this::MESSAGES_PER_USER; $j++) {
            $this->messages[] = [
                'sender_id'   => $authors->sender(),
                'receiver_id' => $authors->receiver(),
                'message'     => $this->getMessage(),
                'created_at'  => $this->getDate()
            ];

            $authors->shuffle();
        }
    }

    private function getMessage(): string
    {
        return $this->faker->text($this->faker->numberBetween(30, 200));
    }

    private function getDate(): string
    {
        return $this->faker->dateTimeBetween('-1 week', 'now')->format('Y-m-d H:i:s');
    }

    private function getAuthors(): object
    {
        return new class($this->users)
        {
            private $users;
            /** @var string */
            private $sender;
            /** @var string */
            private $receiver;

            public function __construct(array $users)
            {
                $this->users = $users;

                $this->setSender()->setReceiver($this->sender);
            }

            public function sender(): string
            {
                return $this->sender;
            }

            public function receiver(): string
            {
                return $this->receiver;
            }

            public function shuffle(): object
            {
                if (\random_int(0, 1) === 1) {
                    $tmp            = $this->sender;
                    $this->sender   = $this->receiver;
                    $this->receiver = $tmp;
                }

                return $this;
            }

            private function setSender(): object
            {
                $this->sender = $this->getUser();
                return $this;
            }

            private function setReceiver(string $sender): object
            {
                $receiver = $this->getUser();

                if ($sender === $receiver) {
                    $this->setReceiver($sender);
                } else {
                    $this->receiver = $receiver;
                }

                return $this;
            }

            private function getUser(): string
            {
                return $this->users[\array_rand($this->users)];
            }
        };
    }
}
