<?php

declare(strict_types = 1);

include __DIR__ . '/../../../vendor/autoload.php';

use App\Application\Services\Search\RequestParser\TagsFactory;
use App\Application\Services\SerializeService;
use Phinx\Seed\AbstractSeed;

class ProfileValueSeeder extends AbstractSeed
{
    private static $rules = [
        'gender'      => 'one',
        'tattoo'      => 'one',
        'zodiac'      => 'one',
        'alcohol'     => 'one',
        'language'    => 'many',
        'religion'    => 'one',
        'eye_color'   => 'one',
        'hair_color'  => 'one',
        'nationality' => 'one'
    ];

    public function run(): void
    {
        $faker = Faker\Factory::create();

        $profileTemplate = (string) $this->fetchRow(
            'select properties from dictionaries.profile limit 1'
        )['properties'];

        $users    = $this->fetchAll('select id from users.users');
        $profile  = SerializeService::deserialize($profileTemplate);
        $profile  = array_map('array_keys', $profile);
        $profiles = [];

        foreach ($users as $user) {
            $properties = [];

            foreach ($profile as $key => $property) {
                $properties[$key] = $this->randomValue($key, $property);
            }

            $birthDate         = $faker->dateTimeBetween('-50 years', '-18 years');
            $properties['age'] = [$birthDate->diff(new DateTime())->y];

            $profiles[] = [
                'user_id'    => $user['id'],
                'name'       => ucfirst($faker->userName),
                'birthdate'  => $birthDate->format('Y-m-d'),
                'properties' => TagsFactory::tags($properties)
            ];
        }

        $this->table('users.profiles')->insert($profiles)->save();
    }

    private function randomValue($key, $values): array
    {
        $amount = self::$rules[$key] ?? 'one';
        $result = [];

        if ($amount === 'one') {
            $randIndex = array_rand($values);
            return [$randIndex => $values[$randIndex]];
        }

        for ($i = 0, $iMax = random_int(0, count($values) - 1); $i < $iMax; $i++) {
            $result[$i] = $values[$i];
        }

        return $result;
    }

}
