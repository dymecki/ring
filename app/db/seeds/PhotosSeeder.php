<?php

declare(strict_types = 1);

include __DIR__ . '/../../../vendor/autoload.php';

use Phinx\Seed\AbstractSeed;

class PhotosSeeder extends AbstractSeed
{
    const MAX_PHOTOS_PER_USER = 10;

    public function run(): void
    {
        $path   = __DIR__ . '/../../../resources/photos/';
        $users  = $this->fetchAll('SELECT id FROM users.users');
        $dir    = new FilesystemIterator($path);
        $photos = [];

        foreach ($dir as $photo) {
            if ( ! $users) {
                break;
            }

            [$width, $height] = getimagesize($path . $photo->getFilename());

            $photos[] = [
                'user_id'   => array_pop($users)['id'],
                'path'      => $photo->getFilename(),
                'extension' => $photo->getExtension(),
                'size'      => $photo->getSize(),
                'width'     => $width,
                'height'    => $height
            ];
        }

        $this->table('resources.photos')->insert($photos)->save();
    }

}
