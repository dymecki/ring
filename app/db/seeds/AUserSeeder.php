<?php

include __DIR__ . '/../../../vendor/autoload.php';

use Phinx\Seed\AbstractSeed;
use App\Application\Services\PasswordHash;

class AUserSeeder extends AbstractSeed
{
    public function run(): void
    {
        $faker = Faker\Factory::create();
        $data  = [
            [
                'name'          => 'Michal',
                'email'         => 'michal@dymecki.com',
                'password_hash' => PasswordHash::hash('Qwert123_')
            ],
            [
                'name'          => 'admin',
                'email'         => 'michal@dymecki.com',
                'password_hash' => PasswordHash::hash('Qwert123_')
            ],
            [
                'name'          => 'SystemTest',
                'email'         => 'michal@dymecki.com',
                'password_hash' => PasswordHash::hash('Qwert123_')
            ]
        ];

        for ($i = 0; $i < 77; $i++) {
            $data[] = [
                'name'          => ucfirst($faker->userName),
                'email'         => $faker->email,
                'password_hash' => PasswordHash::hash($faker->password)
            ];
        }

        $this->table('users.users')->insert($data)->save();
    }

}
