/*

https://stackoverflow.com/a/28849656
For a production system, you can use this configuration :

--ACCESS DB
REVOKE CONNECT ON DATABASE nova FROM PUBLIC;
GRANT  CONNECT ON DATABASE nova  TO user;

--ACCESS SCHEMA
REVOKE ALL     ON SCHEMA public FROM PUBLIC;
GRANT  USAGE   ON SCHEMA public  TO user;

--ACCESS TABLES
REVOKE ALL ON ALL TABLES IN SCHEMA public FROM PUBLIC ;
GRANT SELECT                         ON ALL TABLES IN SCHEMA public TO read_only ;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO read_write ;
GRANT ALL                            ON ALL TABLES IN SCHEMA public TO admin ;

*/

-- Remove "public" schema

create role trader_admin;
create role trader_user;
create role trader_readonly;

grant trader_user     to trader_admin;
grant trader_readonly to trader_user;

revoke all     on database trader from public;
grant  connect on database trader to trader_readonly;

revoke all on schema public from public;

revoke all
    on all tables
    in schema assets, candles, dataexplorer, dataproviders, dictionaries,
              indicators, public, signals, system, users
  from public;

/* trader_readonly grants */
grant usage
   on schema assets, candles, dataexplorer, dataproviders, dictionaries,
             indicators, public, signals, system, users
   to trader_readonly;

grant select
   on all tables
   in schema assets, candles, dataexplorer, dataproviders, dictionaries,
             indicators, public, signals, system, users
   to trader_readonly;

alter default privileges
   in schema "public"
grant select
   on tables
   to trader_readonly;


/* trader_user grants */
grant insert
   on all tables
   in schema system
   to trader_user;

grant insert, update, delete, references
   on all tables
   in schema assets, candles, dataexplorer, dataproviders, dictionaries,
             indicators, public, system, users
   to trader_user;

alter default privileges
   in schema system
grant insert
   on tables
   to trader_user;

alter default privileges
   in schema assets, candles, dataexplorer, dataproviders, dictionaries,
             indicators, public, system, users
grant insert, update, delete, references
   on tables
   to trader_user;

/* trader_admin grants */
grant all
   on all tables
   in schema assets, candles, dataexplorer, dataproviders, dictionaries,
             indicators, public, signals, system, users
   to trader_user;

create user trader_webapp with password 'secret';
create user trader_cli    with password 'secret';

grant trader_user  to trader_webapp;
grant trader_admin to trader_cli;
