<?php

declare(strict_types = 1);

final class Registry
{
    private static $instance;
    private $registry;

    private function __construct()
    {
        $this->registry = [];
    }

    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add(string $name, $item): void
    {
        $this->registry[$name] = $item;
    }

    public function get(string $name)
    {
        if (!isset($this->registry[$name])) {
            throw new \Exception('No such item in the registry: ' . $name);
        }

        return $this->registry[$name];
    }

}
