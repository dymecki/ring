<?php

use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$logger  = new Logger('DataMixer');
$handler = new StreamHandler(__DIR__ . '/../log/php_errors.log', Logger::DEBUG);

ErrorHandler::register($logger);

$lineFormatter = new LineFormatter;
$lineFormatter->includeStacktraces();
$handler->setFormatter($lineFormatter);
$logger->pushHandler($handler);
