<?php

declare(strict_types = 1);

require_once '../vendor/autoload.php';
require_once 'Registry.php';
require_once 'PostgreSqlHandler.php';
require_once 'UserIdProcessor.php';

use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Registry as MonologRegistry;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use App\Persistence\Connection\Db;

/**
 * Logging
 */
//$whoops = new \Whoops\Run;
//$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
//$whoops->register();

$pdo    = Db::instance();
$logger = new Logger('system');

$lineFormatter = new LineFormatter(
    "[%datetime%] [%level_name%] =================\n%message%\n\n%context%\n%extra%\n\n", 'd.m.Y H:i:s'
);
$lineFormatter->includeStacktraces();

$handlerFile     = new StreamHandler(__DIR__ . '/../log/php_errors.log');
$handlerSql      = new PostgreSqlHandler($pdo, \Monolog\Logger::DEBUG);
$handlerTerminal = new StreamHandler('php://stdout', \Monolog\Logger::DEBUG);
$handlerTerminal->setFormatter($lineFormatter);

$logger->pushHandler($handlerFile);
$logger->pushHandler($handlerSql);
$logger->pushHandler($handlerTerminal);
$logger->pushProcessor(new Monolog\Processor\WebProcessor());
$logger->pushProcessor(new Monolog\Processor\MemoryUsageProcessor());
$logger->pushProcessor(new Monolog\Processor\ProcessIdProcessor());
$logger->pushProcessor(new \JK\Monolog\Processor\RequestHeaderProcessor());

ErrorHandler::register($logger);

//$whoops->pushHandler(function($exception, $inspector, $run) use ($logger) {
//    $logger->addError($exception->getMessage());
//});
//exit;
/**
 * Doctrine
 */
// Create a simple "default" Doctrine ORM configuration for Annotations
//$isDevMode = true;
//$config    = Setup::createYAMLMetadataConfiguration(
//        [dirname(__DIR__) . '/Config/Doctrine/Yaml'], $isDevMode
//);

$conn = [
    'driver'   => 'pdo_pgsql',
    'user'     => 'homestead',
    'password' => 'secret',
    'dbname'   => 'ring',
    'host'     => 'localhost',
    'port'     => '5432'
];

//$driver = new YamlDriver('../Config/Doctrine/Yaml/config.yml');
//$entityManager = EntityManager::create($conn, $config);
//Registry::getInstance()->add('EntityManager', EntityManager::create($conn, $config));
//Registry::getInstance()->add('Logger', $whoops);

/**
 * Templates
 */
//$loader = new FilesystemLoader(__DIR__ . '/../src/Output/Templates/');
//$twig   = new Environment($loader,
//    [
//    'debug'       => true,
//    'auto_reload' => true
//    ]);
