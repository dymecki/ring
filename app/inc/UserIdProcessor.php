<?php

declare(strict_types = 1);

class UserIdProcessor
{
    private $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function __invoke(array $record): array
    {
        $record['extra']['user_id'] = $this->userId;

        return $record;
    }

}
