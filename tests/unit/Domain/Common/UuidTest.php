<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use App\Domain\Common\Uuid;
use PHPUnit\Framework\TestCase;

final class UuidTest extends TestCase
{
    public function testUuidProperFormat()
    {
        $uuid = Uuid::create();
        $this->assertTrue(\Ramsey\Uuid\Uuid::isValid($uuid));
    }
}
