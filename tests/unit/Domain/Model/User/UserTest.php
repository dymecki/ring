<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User\User;

final class UserTest extends TestCase
{
    private $user;

    public function setUp(): void
    {
        $this->user = User::register(
            '8330f4e3-9443-4450-949f-a0aa1d4cab2b',
            'John',
            'john@yahoo.com'
        );
    }

    public function testCanBeRegistered(): void
    {
        $this->assertInstanceOf(User::class, $this->user);
    }

    public function testCannotBeRegisteredWithWrongId(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        User::register('10', 'John', 'john@yahoo.com');
    }

    public function testConstructorIsPrivate(): void
    {
        $reflection = new \ReflectionMethod(User::class, '__construct');
        $this->assertTrue($reflection->isPrivate());
    }

    public function testName(): void
    {
        $this->assertSame('John', (string) $this->user->name());
    }

    public function testEmail(): void
    {
        $this->assertSame('john@yahoo.com', (string) $this->user->email());
    }

    public function testId(): void
    {
        $this->assertSame('8330f4e3-9443-4450-949f-a0aa1d4cab2b', (string) $this->user->id());
    }

}
