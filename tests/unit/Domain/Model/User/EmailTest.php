<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User\Email;

final class EmailTest extends TestCase
{
    public function testCanBeCreatedFromValidEmail(): void
    {
        $this->assertInstanceOf(Email::class, new Email('foo@yahoo.com'));
    }

    public function testCannotBeCreatedFromInvalidEmail(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new Email('foo@yahoo');
    }

    public function testToString(): void
    {
        $this->assertSame('foo@yahoo.com', (string) new Email('foo@yahoo.com'));
    }

    public function testEmailsAreEqual(): void
    {
        $emailA = new Email('foo@yahoo.com');
        $emailB = new Email('foo@yahoo.com');

        $this->assertTrue($emailA->equals($emailB));
    }

    public function testEmailsAreNotEqual(): void
    {
        $emailA = new Email('foo@yahoo.com');
        $emailB = new Email('bar@yahoo.com');

        $this->assertFalse($emailA->equals($emailB));
    }

}
