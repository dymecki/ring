<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User\UserPassword;
use App\Domain\Model\User\Exceptions\InvalidPasswordFormatException;

final class UserPasswordTest extends TestCase
{
    public function testCanBeCreatedFromValidName(): void
    {
        $this->assertInstanceOf(
            UserPassword::class,
            new UserPassword('Qwerty12_')
        );
    }

    public function testCanBeCreatedFromValidName2(): void
    {
        $this->assertInstanceOf(
            UserPassword::class,
            new UserPassword('Qwerty12!')
        );
    }

    public function testCannotBeCreatedFromNumber(): void
    {
        $this->expectException(InvalidPasswordFormatException::class);

        new UserPassword('10');
    }

    public function testPasswordsEqual(): void
    {
        $passwordA = new UserPassword('Qwert123_');
        $passwordB = new UserPassword('Qwert123_');

        $this->assertTrue($passwordA->equals($passwordB));
    }

    public function testPasswordsNotEqual(): void
    {
        $passwordA = new UserPassword('Qwert123_');
        $passwordB = new UserPassword('Qwert123*');

        $this->assertFalse($passwordA->equals($passwordB));
    }

}
