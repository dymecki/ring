<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User\UserName;

final class UserNameTest extends TestCase
{
    public function testCanBeCreatedFromValidName(): void
    {
        $this->assertInstanceOf(UserName::class, new UserName('John'));
    }

    public function testCannotBeTooShort(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new UserName('A');
    }

//    public function testCannotContainNumber(): void
//    {
//        $this->expectException(\InvalidArgumentException::class);
//        new UserName('John10');
//    }

    public function testToString(): void
    {
        $this->assertSame('John', (string) new UserName('John'));
    }

}
