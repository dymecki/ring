<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User\UserId;

final class UserIdTest extends TestCase
{
    public function testCanBeCreatedFromValidUuid(): void
    {
        $this->assertInstanceOf(
            UserId::class,
            new UserId('8330f4e3-9443-4450-949f-a0aa1d4cab2b')
        );
    }

    public function testCannotBeCreatedFromNumber(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new UserId('10');
    }

}
