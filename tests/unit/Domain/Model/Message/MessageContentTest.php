<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use App\Domain\Model\Conversation\MessageContent;
use PHPUnit\Framework\TestCase;

final class MessageContentTest extends TestCase
{
    public function testCanBeCreatedFromValidContent(): void
    {
        $this->assertInstanceOf(MessageContent::class, new MessageContent('Sample text'));
    }

    public function testCannotBeEmpty(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new MessageContent('');
    }

    public function testToString(): void
    {
        $this->assertSame('Sample text', (string) new MessageContent('Sample text'));
    }

}
