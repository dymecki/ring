<?php

declare(strict_types = 1);

use App\CommandBus\CommandBusFactory;
use App\CommandBus\SimpleCommandBus;
use PHPUnit\Framework\TestCase;

class CommandBusFactoryTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testBuild()
    {
        $commandBus = CommandBusFactory::build();
        $this->assertInstanceOf(SimpleCommandBus::class, $commandBus);
    }

}
