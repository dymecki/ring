<?php

declare(strict_types = 1);

use App\Application\Commands\LoginCommand;
use App\Application\Commands\LoginCommandHandler;
use App\CommandBus\Command;
use App\CommandBus\SimpleCommandBus;
use PHPUnit\Framework\TestCase;
//use Mockery;

class SimpleCommandBusTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

//    public function testCommandBus()
//    {
//        $command = Mockery::mock('TestCommand, ' . Command::class);
//        $command->shouldReceive('getLogin')->andReturn('foo');
//        $command->shouldReceive('commandName')->andReturn('Test');
//
//        $handler = Mockery::mock('overload:TestCommandHandler');
////        $handler = Mockery::mock('overload:TestCommandHandler, ' . Handler::class);
//        $handler->shouldReceive('handle')->with($command);
//
//        $commandBus = Mockery::mock('overload:SimpleCommandBus');
//        $commandBus->shouldReceive('handlerFactory')->once()->withAnyArgs()->andReturn($handler);
//        $commandBus->shouldReceive('handle')->once()->with($command);
//        $commandBus->handle($command);
//    }

    public function testClassName()
    {
        $command    = new LoginCommand('foo', 'bar');
        $commandBus = new SimpleCommandBus();
        $className  = $commandBus->handlerClass($command);

        $this->assertSame(LoginCommandHandler::class, $className);
    }

    public function testGetHandler()
    {
        $commandBus = new SimpleCommandBus();
        $handler    = $commandBus->handlerFactory(LoginCommandHandler::class);

        $this->assertInstanceOf(LoginCommandHandler::class, $handler);
    }

}
