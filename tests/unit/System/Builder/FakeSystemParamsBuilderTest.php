<?php

declare(strict_types = 1);

use App\System\Builder\FakeSystemParamsBuilder;
use App\System\Builder\SystemBuilder;
use App\System\Builder\SystemParams;
use PHPUnit\Framework\TestCase;

class FakeSystemParamsBuilderTest extends TestCase
{
    /** @var SystemBuilder */
    private $builder;

    public function setUp()
    {
        $this->builder = new FakeSystemParamsBuilder();
        $this->builder->setAuth();
        $this->builder->setCommandBus();
        $this->builder->setData();
        $this->builder->setLoggers();
        $this->builder->setConfig();
        $this->builder->setDb();
    }

    public function testCanBuildSystemParams()
    {
        $this->assertInstanceOf(SystemParams::class, $this->builder->params());
    }

}
