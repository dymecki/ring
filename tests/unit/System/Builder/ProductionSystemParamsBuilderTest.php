<?php

declare(strict_types = 1);

use App\System\Builder\ProductionSystemParamsBuilder;
use App\System\Builder\SystemBuilder;
use App\System\Builder\SystemParams;
use PHPUnit\Framework\TestCase;

class ProductionSystemParamsBuilderTest extends TestCase
{
    /** @var SystemBuilder */
    private $builder;

    public function setUp()
    {
        $this->builder = new ProductionSystemParamsBuilder();
        $this->builder->setAuth();
        $this->builder->setCommandBus();
        $this->builder->setData();
        $this->builder->setLoggers();
        $this->builder->setConfig();
        $this->builder->setDb();
    }

    public function testCanBuildSystemParams()
    {
        $this->assertInstanceOf(SystemParams::class, $this->builder->params());
    }

}
