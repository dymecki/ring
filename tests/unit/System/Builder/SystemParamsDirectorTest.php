<?php

declare(strict_types = 1);

use App\System\Builder\FakeSystemParamsBuilder;
use App\System\Builder\ProductionSystemParamsBuilder;
use App\System\Builder\SystemParams;
use App\System\Builder\SystemParamsDirector;
use PHPUnit\Framework\TestCase;

class SystemParamsDirectorTest extends TestCase
{
    public function testCanBuildProductionParams()
    {
        $productionParamsBuilder = new ProductionSystemParamsBuilder();
        $params = SystemParamsDirector::build($productionParamsBuilder);

        $this->assertInstanceOf(SystemParams::class, $params);
    }

    public function testCanBuildFakeParams()
    {
        $productionParamsBuilder = new FakeSystemParamsBuilder();
        $params = SystemParamsDirector::build($productionParamsBuilder);

        $this->assertInstanceOf(SystemParams::class, $params);
    }
}
