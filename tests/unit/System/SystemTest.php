<?php

declare(strict_types = 1);

use App\System\System;
use PHPUnit\Framework\TestCase;

class SystemTest extends TestCase
{
    public function testCanBuildSystem()
    {
        $this->assertInstanceOf(System::class, System::run());
    }

}
