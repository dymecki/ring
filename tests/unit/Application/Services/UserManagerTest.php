<?php

declare(strict_types = 1);

namespace App\Tests\Domain\Model\User;

use App\Application\Services\UserManager;
use App\Domain\Model\User\Email;
use App\Domain\Model\User\UserName;
use App\Domain\Model\User\UserPassword;
use App\Persistence\Dao\UserDao;
use DG\BypassFinals as MockFinalClass;
use Mockery;
use PHPUnit\Framework\TestCase;

final class UserManagerTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function setUp()
    {
        MockFinalClass::enable();
    }

    public function testCannotRegisterUserWhenPasswordsDoNotMatch(): void
    {
        $userDao = Mockery::mock(UserDao::class);
        $userDao->shouldReceive('userExists')->withAnyArgs()->andReturn(false);
        $userDao->shouldReceive('add');

        $this->expectException(\InvalidArgumentException::class);

        (new UserManager($userDao))->register(
            new UserName('John'),
            new Email('john@yahoo.com'),
            new UserPassword('Qwerty123_'),
            new UserPassword('Qwerty456_')
        );
    }

    public function testCannotRegisterAnUserThatAlredyExists(): void
    {
        $userDao = Mockery::mock(UserDao::class);
        $userDao->shouldReceive('userExists')->withAnyArgs()->andReturn(true);
        $userDao->shouldReceive('add');

        $this->expectException(\Exception::class);

        (new UserManager($userDao))->register(
            new UserName('John'),
            new Email('john@yahoo.com'),
            new UserPassword('Qwerty123_'),
            new UserPassword('Qwerty123_')
        );
    }

}
