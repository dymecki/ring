<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use App\Domain\Common\Uuid;
use App\Domain\Model\Conversation\Exceptions\MessageInvalidReceiverException;
use App\Domain\Model\Conversation\Message;
use App\Domain\Model\Conversation\MessageContent;
use App\Domain\Model\Conversation\MessageId;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserId;
use PHPUnit\Framework\TestCase;

final class MessageServiceTest extends TestCase
{
    /** @var Message */
    private $message;
    private $uuidA;
    private $uuidB;
    private $content;
    private $datetimeStr;
    /** @var \DateTime */
    private $datetime;

    public function setUp(): void
    {
        $this->uuidA       = Uuid::create();
        $this->uuidB       = Uuid::create();
        $this->content     = 'Lorem ipsum dolor';
        $this->datetimeStr = '2018-10-03 14:12:10';
        $this->datetime    = new \DateTime($this->datetimeStr);

        $this->message = Message::write(
            $this->uuidA,
            $this->uuidB,
            $this->content,
            $this->datetimeStr
        );
    }

    public function testCanBeWritten(): void
    {
        $this->assertInstanceOf(Message::class, $this->message);
    }

    public function testSenderCannotSendAMessageToHimself(): void
    {
        $this->expectException(MessageInvalidReceiverException::class);
        Message::write($this->uuidA, $this->uuidA, $this->content, $this->datetimeStr);
    }

    public function testCannotBeCreatedWithWrongId(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Message::write('10', '123', $this->content, $this->datetimeStr);
    }

    public function testConstructorIsPrivate(): void
    {
        $reflection = new \ReflectionMethod(User::class, '__construct');
        $this->assertTrue($reflection->isPrivate());
    }

    public function testSenderId(): void
    {
        $this->assertInstanceOf(UserId::class, $this->message->senderId());
        $this->assertSame($this->uuidA, (string) $this->message->senderId());
        $this->assertEquals(new UserId($this->uuidA), $this->message->senderId());
    }

    public function testReceiverId(): void
    {
        $this->assertInstanceOf(UserId::class, $this->message->receiverId());
        $this->assertSame($this->uuidB, (string) $this->message->receiverId());
        $this->assertEquals(new UserId($this->uuidB), $this->message->receiverId());
    }

    public function testContent(): void
    {
        $this->assertInstanceOf(MessageContent::class, $this->message->content());
        $this->assertSame($this->content, (string) $this->message->content());
        $this->assertEquals(new MessageContent($this->content), $this->message->content());
    }

    public function testDateTime(): void
    {
        $orgDatetime = $this->datetime->format('Y-m-d H:i:s');
        $msgDatetime = $this->message->datetime()->format('Y-m-d H:i:s');

        $this->assertInstanceOf(\DateTimeImmutable::class, $this->message->datetime());
        $this->assertSame($orgDatetime, $msgDatetime);
        $this->assertEquals($this->datetime, $this->message->datetime());
    }

    public function testId(): void
    {
        $this->assertInstanceOf(MessageId::class, $this->message->id());
        $this->assertTrue(\Ramsey\Uuid\Uuid::isValid($this->message->id()));
    }

}
