<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model\User;

use App\Application\Commands\WriteMessageCommand;
use App\Application\Commands\WriteMessageCommandHandler;
use App\Application\Services\MessageService;
use App\Domain\Common\Uuid;
use PHPUnit\Framework\TestCase;

final class WriteMessageCommandHandlerTest extends TestCase
{
    private $uuidA;
    private $uuidB;
    private $content;
    /** @var WriteMessageCommand */
    private $command;

    public function setUp(): void
    {
        $this->uuidA       = Uuid::create();
        $this->uuidB       = Uuid::create();
        $this->content     = 'Lorem ipsum dolor';

        $this->command = new WriteMessageCommand(
            $this->uuidA,
            $this->uuidB,
            $this->content
        );
    }

    public function testContentIsProper(): void
    {
        $this->assertTrue($this->command->getMessage() === $this->content);
    }

//    public function testCommandCanBeHandle(): void
//    {
//        $this->markTestSkipped('must be revisited.');
//
//        $messageService = \Mockery::mock(MessageService::class);
//        $messageService->shouldReceive('send')->withAnyArgs();
//
//        (new WriteMessageCommandHandler())->handle($this->command);
//    }

}
