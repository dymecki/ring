<?php

declare(strict_types = 1);

require_once 'vendor/autoload.php';

function fixCoverageDirectories(string $src, string $dest): void
{
    if ( ! file_exists($dest)) {
        mkdir($dest, 0755);
    }

    $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($src, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST
    );

    foreach ($iterator as $item) {
        $path = $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName();

        if ($item->isDir() && ! file_exists($path)) {
            //            echo 'Path: ' . $path . PHP_EOL;
            mkdir($path, 0755);
        }
    }
}

fixCoverageDirectories(__DIR__ . '/../src', __DIR__ . '/../build/coverage/xml');
fixCoverageDirectories(__DIR__ . '/../src', __DIR__ . '/../build/infection/coverage-xml');
DG\BypassFinals::enable();