<?php

declare(strict_types = 1);

namespace App\Tests\Integration;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

final class SearchTest extends TestCase
{
    public function testLoggedUserCanSeeSearchPage()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';

        $client->submit($form);
        $client->request('get', 'http://ring.local/panel');

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/panel', $client->getInternalRequest()->getUri());
    }

    public function testUnloggedUserCannotSeeSearchPage()
    {
        $client = new Client();
        $client->request('get', 'http://ring.local/search');

        $this->assertNotEquals(302, $client->getInternalResponse()->getStatus());
    }

//    public function testLoggedUserCanSubmitSearchForm()
//    {
//        $client           = new Client();
//        $crawler          = $client->request('get', 'http://ring.local/login');
//        $form             = $crawler->selectButton('Log in')->form();
//        $form['login']    = 'Michal';
//        $form['password'] = 'Qwert123_';
//        $client->submit($form);
//
//        $crawler = $client->request('get', 'http://ring.local/panel');
//        $form = $crawler->selectButton('Find')->form();
//        $client->submit($form);
//
//        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
//        $this->assertEquals('http://ring.local/search', $client->getInternalRequest()->getUri());
//    }

}