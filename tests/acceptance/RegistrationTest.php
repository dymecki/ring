<?php

declare(strict_types = 1);

namespace App\Tests\Integration;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

final class RegistrationTest extends TestCase
{
    public function testHomepageRegistrationLink()
    {
        $client  = new Client();
        $crawler = $client->request('get', 'http://ring.local');
        $link    = $crawler->selectLink('Register')->link();
        $client->click($link);

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/registration', $client->getInternalRequest()->getUri());
    }

    public function testRegistrationPage()
    {
        $client            = new Client();
        $crawler           = $client->request('get', 'http://ring.local/registration');
        $form              = $crawler->selectButton('Register')->form();
        $form['name']      = '';
        $form['email']     = '';
        $form['password']  = '';
        $form['password2'] = '';
        $client->submit($form);
        //        $crawler = $client->request('get')

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals(
            'http://ring.local/registration/confirmation',
            $client->getInternalRequest()->getUri()
        );
    }

    public function testLoggedUserCannotSeeRegistrationPage()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';
        $client->submit($form);
        $client->request('get', 'http://ring.local/registration');

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/messages', $client->getInternalRequest()->getUri());
    }
}