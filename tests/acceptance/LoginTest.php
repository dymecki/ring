<?php

declare(strict_types = 1);

namespace App\Tests\Integration;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

final class LoginTest extends TestCase
{
    public function testHomepageLoginLink()
    {
        $client  = new Client();
        $crawler = $client->request('get', 'http://ring.local');
        $link    = $crawler->selectLink('Login')->link();
        $client->click($link);

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/login', $client->getInternalRequest()->getUri());
    }

    public function testLoginPage()
    {
        $client  = new Client();
        $client->request('get', 'http://ring.local/login');

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/login', $client->getInternalRequest()->getUri());
    }

    public function testLoginToTheSystem()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';
        $crawler          = $client->submit($form);
        $crawler          = $client->request('get', 'http://ring.local/messages');

        $this->assertTrue($crawler->filter('div[class="messages"]')->count() === 1);
        $this->assertEquals('http://ring.local/messages', $client->getInternalRequest()->getUri());
    }

    public function testLogoutFromTheSystem()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';
        $crawler          = $client->submit($form);
        $crawler          = $client->request('get', 'http://ring.local/messages');

        $this->assertTrue($crawler->filter('div[class="messages"]')->count() === 1);
        $client->request('get', 'http://ring.local/logout');
        $this->assertEquals('http://ring.local/', $client->getInternalRequest()->getUri());
    }

    public function testFailedLoginToTheSystemWithWrongPassword()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert456_';
        $crawler          = $client->submit($form);
        $crawler          = $client->request('get', 'http://ring.local/messages');

        $this->assertTrue($crawler->filter('div[class="messages"]')->count() === 0);
        $this->assertEquals('http://ring.local/login', $client->getInternalRequest()->getUri());
    }

    public function testLoggedUserCannotSeeLoginPage()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';

        $client->submit($form);
        $client->request('get', 'http://ring.local/login');
        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/messages', $client->getInternalRequest()->getUri());
    }

}