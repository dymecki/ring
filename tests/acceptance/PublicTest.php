<?php

declare(strict_types = 1);

namespace App\Tests\Integration;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

final class PublicTest extends TestCase
{
    public function testHomepage()
    {
        $client  = new Client();
        $client->request('get', 'http://ring.local');

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local', $client->getInternalRequest()->getUri());
    }

    public function testHomepageLogoLink()
    {
        $client  = new Client();
        $crawler = $client->request('get', 'http://ring.local/login');
        $link    = $crawler->filter('#logo')->link();
        $client->click($link);

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/', $client->getInternalRequest()->getUri());
    }

}