<?php

declare(strict_types = 1);

namespace App\Tests\Integration;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

final class PanelTest extends TestCase
{
    public function testLoggedUserCanSeePanelPage()
    {
        $client           = new Client();
        $crawler          = $client->request('get', 'http://ring.local/login');
        $form             = $crawler->selectButton('Log in')->form();
        $form['login']    = 'Michal';
        $form['password'] = 'Qwert123_';

        $client->submit($form);
        $client->request('get', 'http://ring.local/panel');

        $this->assertEquals(200, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/panel', $client->getInternalRequest()->getUri());
    }

    public function testUnloggedUserCannotSeePanelPage()
    {
        $client = new Client();
        $client->request('get', 'http://ring.local/panel');

        $this->assertNotEquals(302, $client->getInternalResponse()->getStatus());
        $this->assertEquals('http://ring.local/login', $client->getInternalRequest()->getUri());
    }

}