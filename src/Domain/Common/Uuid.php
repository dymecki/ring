<?php

declare(strict_types=1);

namespace App\Domain\Common;

final class Uuid
{
    public static function create(): string
    {
        return \Ramsey\Uuid\Uuid::uuid4()->toString();
    }
}
