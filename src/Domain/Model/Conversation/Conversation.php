<?php

declare(strict_types = 1);

namespace App\Domain\Model\Conversation;

final class Conversation implements \Iterator, \Countable
{
    private $me;
    private $friend;
    private $messages;
    private $key = 0;

    public function __construct($me, $friend, array $messages)
    {
        $this->me       = $me;
        $this->friend   = $friend;
        $this->messages = $messages;
    }

    public function add(Message $message): self
    {
        $this->messages[(string) $message->id()] = $message;

        return $this;
    }

    public function count(): int
    {
        return \count($this->messages);
    }

    public function current()
    {
        return $this->messages[$this->key];
    }

    public function key(): int
    {
        return $this->key;
    }

    public function next(): void
    {
        $this->key++;
    }

    public function rewind(): void
    {
        $this->key = 0;
    }

    public function valid(): bool
    {
        return isset($this->messages[$this->key]);
    }

}
