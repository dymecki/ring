<?php

declare(strict_types=1);

namespace App\Domain\Model\Conversation\Events;

use App\Domain\Common\Event;

final class MessageSent implements Event
{

}
