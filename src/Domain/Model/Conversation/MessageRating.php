<?php

declare(strict_types = 1);

namespace App\Domain\Model\Conversation;

final class MessageRating
{
    private $rating;

    private function __construct(int $rating)
    {
        if ($rating < 1 || $rating > 10) {
            throw new \InvalidArgumentException('Rating must in 1-10 range');
        }

        $this->rating = $rating;
    }

    public function value(): int
    {
        return $this->rating;
    }

    public function __toString()
    {
        return (string) $this->value();
    }

}
