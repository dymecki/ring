<?php

declare(strict_types = 1);

namespace App\Domain\Model\Conversation\Exceptions;

final class MessageInvalidReceiverException extends \Exception
{

}