<?php

declare(strict_types=1);

namespace App\Domain\Model\Conversation;

use App\Domain\Common\Uuid;
use App\Domain\Model\Conversation\Exceptions\MessageInvalidReceiverException;
use App\Domain\Model\User\UserId;

final class Message
{
    private $id;
    private $senderId;
    private $receiverId;
    private $content;
    private $datetime;

    private function __construct(
        MessageId $id,
        UserId $senderId,
        UserId $receiverId,
        MessageContent $content,
        \DateTimeImmutable $datetime = null
    ){
        if ($senderId->equals($receiverId)) {
            throw new MessageInvalidReceiverException(
                'Message cannot be sent to its sender'
            );
        }

        $this->id         = $id;
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->content    = $content;
        $this->datetime   = $datetime;
    }

    public static function write(
        string $senderId,
        string $receiverId,
        string $content,
        string $datetime
    ): self {
        return new self(
            new MessageId(Uuid::create()),
            new UserId($senderId),
            new UserId($receiverId),
            new MessageContent($content),
            new \DateTimeImmutable($datetime)
        );
    }

    public function id(): MessageId
    {
        return $this->id;
    }

    public function senderId(): UserId
    {
        return $this->senderId;
    }

    public function receiverId(): UserId
    {
        return $this->receiverId;
    }

    public function content(): MessageContent
    {
        return $this->content;
    }

    public function datetime(): \DateTimeImmutable
    {
        return $this->datetime;
    }

    public function __toString()
    {
        return (string) $this->content();
    }

}
