<?php

declare(strict_types = 1);

namespace App\Domain\Model\Conversation;

final class MessageId
{
    private $value;

    public function __construct(string $uuid)
    {
        if (\strlen($uuid) < 30) {
            throw new \InvalidArgumentException("Invalid message id: $uuid");
        }

        // TODO: check if is a proper uuid value

        $this->value = $uuid;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string) $this->value();
    }

}
