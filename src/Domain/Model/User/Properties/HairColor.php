<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class HairColor extends BaseUserProperty
{
    public const BROWN  = 0;
    public const BLOND  = 1;
    public const BLACK  = 2;
    public const SILVER = 3;
    public const RED    = 4;
    public const BALD   = 5;

    private $value;

    public function __construct(int $value)
    {
        if ( ! \in_array($value, $this->allValues(), true)) {
            throw new \InvalidArgumentException("Unavailable hair color: $value");
        }

        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function allValues(): array
    {
        return [
            self::BROWN, self::BLOND, self::BLACK, self::SILVER, self::RED, self::BALD
        ];
    }
}
