<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class Language extends BaseUserProperty
{
    public const LANGUAGE_ENGLISH = 0;
    public const LANGUAGE_SPANISH = 1;
    public const LANGUAGE_CHINESE = 2;
    public const LANGUAGE_FRENCH  = 3;
    public const LANGUAGE_GERMAN  = 4;
    public const LANGUAGE_ARABIC  = 5;
    public const LANGUAGE_RUSSIAN = 6;

    private $language;

    public function __construct($language)
    {
        $this->language = $language;
    }

    public function value()
    {
        return $this->language;
    }

    public function allValues(): array
    {
        return [
            self::LANGUAGE_ENGLISH,
            self::LANGUAGE_SPANISH,
            self::LANGUAGE_CHINESE,
            self::LANGUAGE_FRENCH,
            self::LANGUAGE_GERMAN,
            self::LANGUAGE_ARABIC,
            self::LANGUAGE_RUSSIAN
        ];
    }

    public function __toString()
    {
        return (string) $this->language;
    }

}
