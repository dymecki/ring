<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class Gender extends BaseUserProperty
{
    public const MALE   = 'm';
    public const FEMALE = 'w';

    private $value;

    public function __construct(string $value)
    {
        if ( ! $this->isProperValue($value)) {
//        if ($value !== $this::MALE && $value !== $this::FEMALE) {
            throw new \InvalidArgumentException("Wrong gender id: $value");
        }

        $this->value = $value;
    }

    public function availableValues(): array
    {
        return [
            self::MALE,
            self::FEMALE
        ];
    }

    public function isProperValue($value): bool
    {
        return in_array($value, $this->availableValues());
    }

    public function __toString()
    {
        return $this->value;
    }
}
