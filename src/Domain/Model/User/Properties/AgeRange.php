<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class AgeRange extends BaseUserProperty
{
    private $min;
    private $max;

    private function __construct(Age $min, Age $max)
    {
        if ($max < $min) {
            throw new \InvalidArgumentException('Min age cannot be grater than max age.');
        }

        $this->min = $min;
        $this->max = $max;
    }

    public static function build(int $min, int $max): self
    {
        $minAge = new Age($min ?: Age::AGE_MIN);
        $maxAge = new Age($max ?: Age::AGE_MAX);

        return new self($minAge, $maxAge);
    }

    public function min(): Age
    {
        return $this->min;
    }

    public function max(): Age
    {
        return $this->max;
    }

    public function range(): array
    {
        return range(
            $this->min()->value(),
            $this->max()->value()
        );
    }
}
