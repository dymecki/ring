<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\UserProperty;
use App\Domain\Model\User\BaseUserProperty;

final class HasTatoo extends BaseUserProperty implements UserProperty
{

}
