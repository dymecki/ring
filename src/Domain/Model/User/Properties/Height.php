<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class Height extends BaseUserProperty
{
    public const HEIGHT_MIN = 100;
    public const HEIGHT_MAX = 230;

    private $value;

    public function __construct(int $value)
    {
        // centimeters
        if ($value < self::HEIGHT_MIN || $value > self::HEIGHT_MAX) {
            throw new \OutOfBoundsException("Height value $value out of bounds");
        }

        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function allValues(): array
    {
        return range(100, 230);
    }

    public function __toString()
    {
        return $this->value . ' cm';
    }

}
