<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Properties;

use App\Domain\Model\User\BaseUserProperty;

final class Age extends BaseUserProperty
{
    public const AGE_MIN = 13;
    public const AGE_MAX = 110;

    private $value;

    public function __construct(int $value)
    {
        if ($value < self::AGE_MIN) {
            throw new \InvalidArgumentException(
                "Age value ($value) is too small. Must be 13 at least."
            );
        }

        if ($value > self::AGE_MAX) {
            throw new \InvalidArgumentException(
                "Age value ($value) is too high. Must be max 110."
            );
        }

        $this->value = $value;
    }

    public function isOlder(self $age): bool
    {
        return $this->value > $age->value;
    }

    public function isYounger(self $age): bool
    {
        return $this->value < $age->value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function allValidValues(): array
    {
        return range(self::AGE_MIN, self::AGE_MAX);
    }

    public function __toString()
    {
        return (string) $this->value;
    }

}
