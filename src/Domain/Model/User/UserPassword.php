<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

use App\Domain\Model\User\Exceptions\InvalidPasswordFormatException as Ex;

final class UserPassword
{
    private $value;

    public function __construct(string $password)
    {
        $this->checkLength($password);
        $this->checkUpperCaseLetter($password);
        $this->checkLowerCaseLetter($password);
        $this->checkDigit($password);
        $this->checkSpecialCharacter($password);

        $this->value = $password;
    }

    private function checkLength(string $password): void
    {
        if (strlen($password) < 8) {
            throw new Ex('Password must be minimum 8 characters long');
        }

        if (strlen($password) > 32) {
            throw new Ex('Password must be maximum 32 characters long');
        }
    }

    private function checkUpperCaseLetter(string $password): void
    {
        if (strtolower($password) === $password) {
            throw new Ex('Password must contain at least one uppercase letter');
        }
    }

    private function checkLowerCaseLetter(string $password): void
    {
        if (strtoupper($password) === $password) {
            throw new Ex('Password must contain at least one lowercase letter');
        }
    }

    private function checkDigit(string $password): void
    {
        if ( ! preg_match('~\d~', $password)) {
            throw new Ex('Password must contain at least one digit');
        }
    }

    private function checkSpecialCharacter(string $password): void
    {
        if ( ! preg_match('/[\'^£$%&*()}{@#~?!><>,|=_+-]/', $password)) {
            throw new Ex('Password must contain at least one special character');
        }
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(UserPassword $password): bool
    {
        return $this->value() === $password->value();
    }

    public function __toString()
    {
        return $this->value();
    }

}
