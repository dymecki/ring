<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

final class UserPersonalityRating
{
    private $value;

    public function __construct(float $rating)
    {
        if ($rating < 1 || $rating > 10) {
            throw new \InvalidArgumentException('User\'s rating value must be in range of 1-10');
        }

        $this->value = $rating;
    }

    public function value(): float
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string) $this->value();
    }

}
