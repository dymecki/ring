<?php

declare(strict_types=1);

namespace App\Domain\Model\User\Events;

use App\Domain\Common\Event;

final class UserRegistered implements Event
{

}
