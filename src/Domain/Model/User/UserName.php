<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

final class UserName
{
    private $value;

    public function __construct(string $name)
    {
        if (\strlen($name) < 2) {
            throw new \InvalidArgumentException("User's name must be at least two characters long");
        }

        //        if ( ! ctype_alpha($name)) {
        //            throw new \InvalidArgumentException("User's name can contain only letters");
        //        }

        $this->value = $name;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }

}
