<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

use App\Domain\Model\User\User;
use App\Domain\Model\User\Users;

final class UsersIterator implements \Iterator, \Countable
{
    private $users;
    private $key = 0;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function add(User $user): self
    {
        $this->users[] = $user;

        return $this;
    }

    public function count(): int
    {
        return count($this->users);
    }

    public function current()
    {
        return $this->users[$this->key];
    }

    public function key(): int
    {
        return $this->key;
    }

    public function next(): void
    {
        $this->key++;
    }

    public function rewind(): void
    {
        $this->key = 0;
    }

    public function valid(): bool
    {
        return isset($this->users[$this->key]);
    }

}
