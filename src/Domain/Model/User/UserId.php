<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

final class UserId
{
    private $value;

    public function __construct(string $uuid)
    {
        if (strlen($uuid) !== 36) {
            throw new \InvalidArgumentException("Invalid user id: $uuid");
        }

        // TODO: check if is a proper uuid value

        $this->value = $uuid;
    }

    public function equals(UserId $userId): bool
    {
        return $this->value() === $userId->value();
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }

}
