<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

final class UserProperties
{
    private $gender;
    private $eyesColor;
    private $hairColor;
}
