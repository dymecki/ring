<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

final class UserBirthDate
{
    private $value;

    public function __construct(string $birthDate)
    {
        // TODO: check min and max age

        $this->value = $birthDate;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }

}
