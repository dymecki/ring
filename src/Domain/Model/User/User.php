<?php

declare(strict_types = 1);

namespace App\Domain\Model\User;

use App\Domain\Model\User\UserName;
use App\Domain\Model\User\UserId;
use App\Domain\Model\User\Email;
use App\Domain\Model\Conversation\Message;

final class User
{
    private $id;
    private $name;
    private $email;
    private $passwordHash;
    private $created_at;

    private function __construct(
        UserId $id,
        UserName $name,
        Email $email,
        $passwordHash = null,
        $createdAt = null
    )
    {
        $this->id           = $id;
        $this->name         = $name;
        $this->email        = $email;
        $this->passwordHash = $passwordHash;
        $this->created_at   = $createdAt;
    }

    public static function register($id, string $name, string $email): self
    {
        return new self(
            new UserId($id),
            new UserName($name),
            new Email($email)
        );
    }

    public static function init($id, string $name, string $email, string $passwordHash, $createdAt): self
    {
        return new self(
            new UserId($id),
            new UserName($name),
            new Email($email),
            $passwordHash,
            $createdAt
        );
    }

    //    public function __set($name, $value): void
    //    {
    //        $name = str_replace('_', '', ucwords($name, '_'));
    //
    //        $mapping = [
    //            'id'    => UserId::class,
    //            'name'  => UserName::class,
    //            'email' => Email::class,
    //
    //        ];
    //
    //        if (isset($this->$name)) {
    //            $this->$name = isset($mapping[$name]) ? new $mapping[$name]($value) : $value;
    //        }
    //    }

    public function __get($name)
    {
    }

    public function id(): UserId
    {
        return $this->id;
    }

    public function name(): UserName
    {
        return $this->name;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function passwordHash(): ?string
    {
        return $this->passwordHash;
    }

    public function createdAt()
    {
        return $this->created_at;
    }

    public function sendMessageTo(User $user)
    {

    }

    public function addPhoto(Image $image)
    {

    }

}
