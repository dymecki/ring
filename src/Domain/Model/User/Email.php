<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

final class Email
{
    private $value;

    public function __construct(string $email)
    {
        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Wrong e-mail address');
        }

        $this->value = $email;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(Email $email): bool
    {
        return $this->value === $email->value();
    }

    public function __toString()
    {
        return $this->value();
    }

}
