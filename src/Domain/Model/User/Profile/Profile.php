<?php

declare(strict_types = 1);

namespace App\Domain\Model\User\Profile;

final class Profile
{
    private $age;
    private $name;
    private $properties;
    private $images;

    public function __construct($age, $name, $properties, $images = null)
    {
        $this->age        = $age;
        $this->name       = $name;
        $this->properties = $properties;
        $this->images     = $images;
    }

    public static function init($age, $name, $properties): self
    {
        return new self($age, $name, $properties);
    }

    public function age()
    {
        return $this->age;
    }

    public function name()
    {
        return $this->name;
    }

    public function properties()
    {
        return $this->properties;
    }

    public function images()
    {
        return $this->images;
    }

}