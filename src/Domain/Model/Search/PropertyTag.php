<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search;

final class PropertyTag
{
    public static function tagName($property, $value): string
    {
        return strtolower($property . '_' . $value);
    }
}