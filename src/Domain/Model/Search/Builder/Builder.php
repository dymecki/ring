<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Builder;

interface Builder
{
    public function setGender(): void;

    public function setLanguage(): void;

    public function query();
}