<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Builder;

final class Director
{
    public static function build(Builder $builder)
    {
        $builder->setGender();
        $builder->setLanguage();

        return $builder->query();
    }
}