<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Factory;

use App\Domain\Model\Search\SearchParam;

final class Factory
{
    public static function build($name, $values)
    {
        $tmp       = explode('_', $name);
        $tmp       = array_map('ucfirst', $tmp);
        $className = join('', $tmp);

        if ( ! class_exists($className)) {
            throw new \InvalidArgumentException("Class $className does not exist");
        }

        return new $className($values);
    }

    /** @return SearchParam[] */
    public function buildParams2($searchRequest): array
    {
        $result = [];

        foreach ($searchRequest as $key => $values) {
            foreach ($values as $value) {
                $result[] = new SearchParam($key, $value);
            }
        }

        return $result;
    }

    /** @return SearchParam[] */
    public function buildParams($searchRequest): array
    {
        $result = [];

        foreach ($searchRequest as $key => $values) {
            $result[] = new SearchParam($key, $values);
        }

        return $result;
    }

    //    public static function build($params)
    //    {
    //        foreach ($params as $key => $value) {
    //            $tmp = explode('_', $key);
    //            $tmp = array_map('ucfirst', $tmp);
    //            $className = join('', $tmp);
    //
    //
    //        }
    //    }
}