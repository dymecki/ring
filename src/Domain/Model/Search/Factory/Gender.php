<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Factory;

use App\Domain\Model\Search\Specification\Specification;

final class Gender
{
    private $specifications;

    public function __construct(Specification ...$specifications)
    {
        $this->specifications = $specifications;
    }

    public function isSatisfiedBy($item): bool
    {
        foreach ($this->specifications as $specification) {
            if ( ! $specification->isSatisfiedBy($item)) {
                return false;
            }
        }

        return true;
    }

}
