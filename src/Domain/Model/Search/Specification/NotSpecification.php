<?php

declare(strict_types=1);

namespace App\Domain\Model\Search\Specification;

final class NotSpecification implements Specification
{
    private $specification;

    public function __construct(Specification ...$specification)
    {
        $this->specification = $specification;
    }

    public function isSatisfiedBy($item): bool
    {
        return !$this->specification->isSatisfiedBy($item);
    }

}
