<?php

declare(strict_types=1);

namespace App\Domain\Model\Search\Specification;

use App\Domain\Model\Search\SearchParam;

interface Specification
{
    public function isSatisfiedBy(SearchParam $item): bool;
}
