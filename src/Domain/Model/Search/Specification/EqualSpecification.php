<?php

declare(strict_types=1);

namespace App\Domain\Model\Search\Specification;

final class EqualSpecification implements Specification
{
    private $specifications;

    public function __construct(Specification ...$specifications)
    {
        $this->specifications = $specifications;
    }

    public function isSatisfiedBy($item): bool
    {
        foreach ($this->specifications as $specification) {
            if (!$specification->isSatisfiedBy($item)) {
                return false;
            }
        }

        return true;
    }

}
