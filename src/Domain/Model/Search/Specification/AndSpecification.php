<?php

declare(strict_types=1);

namespace App\Domain\Model\Search\Specification;

use App\Domain\Model\Search\SearchParam;

final class AndSpecification implements Specification
{
    private $specifications;

    public function __construct(...$specifications)
    {
        $this->specifications = $specifications;
    }

    public function isSatisfiedBy(SearchParam $item): bool
    {
        foreach ($this->specifications as $specification) {
            if (!$specification->isSatisfiedBy($item)) {
                return false;
            }
        }

        return true;
    }

    public function specifications(): array
    {
        return $this->specifications;
    }

}
