<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search;

use App\Domain\Model\Search\Specification\Specification;

final class SearchParam implements Specification
{
    private $name;
    private $values;
    private $operator;

    public function __construct(string $name, $value)
    {
        $this->name     = $name;
        $this->values   = $value;
        $this->operator = null;

        $this->setOperator();
    }

    public function name(): string
    {
        return $this->name;
    }

    public function values()
    {
        return $this->values;
    }

    public function operator()
    {
        return $this->operator;
    }

    public function isSatisfiedBy($item): bool
    {
        return true;
    }

    public function __toString()
    {
        return json_encode([$this->name() => $this->values()]);
    }

    private function setOperator(): void
    {
        if (isset($this->values['operator'])) {
            $this->operator = $this->values['operator'];
            unset($this->values['operator']);
        }
    }
}