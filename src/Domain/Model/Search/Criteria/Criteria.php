<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Criteria;

final class Criteria implements \Countable
{
    private $criteria = [];

    public function add($criterion): self
    {
        $this->criteria[] = $criterion;
        return $this;
    }

    public function count(): int
    {
        return \count($this->criteria);
    }
}