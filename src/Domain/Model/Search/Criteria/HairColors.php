<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Criteria;

use App\Domain\Model\Search\Specification\OrSpecification;
use App\Domain\Model\Search\Specification\Specification;
use App\Domain\Model\User\Properties\HairColor;

final class HairColors
{
    private $hairColors = [];

    private function __construct(int ...$hairColors)
    {
        $this->hairColors = $hairColors;
    }

    public static function build(array $hairColors): Specification
    {
        return new OrSpecification(...$hairColors);
    }

    public function value(): array
    {
        return $this->hairColors;
    }

}