<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Criteria;

use App\Domain\Model\Search\Specification\OrSpecification;
use App\Domain\Model\Search\Specification\Specification;
use App\Domain\Model\User\Properties\Gender;

final class Genders
{
    private $genders;

    public function __construct()
    {
    }

    public static function build(array $formGenders): Specification
    {
        $amount = \count($formGenders);

        if ($amount < 1 && $amount > 2) {
            throw new \InvalidArgumentException('There can be only 1 or 2 genders provided.');
        }

        $genders[] = new Gender($formGenders[0]);

        if (isset($formGenders[1])) {
            $genders[] = new Gender($formGenders[1]);
        }

        return new OrSpecification(...$genders);
    }

}