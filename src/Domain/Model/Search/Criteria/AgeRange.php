<?php

declare(strict_types = 1);

namespace App\Domain\Model\Search\Criteria;

use App\Domain\Model\Search\Specification\OrSpecification;
use App\Domain\Model\Search\Specification\Specification;
use App\Domain\Model\User\Properties\Age;

final class AgeRange
{
    private $minAge;
    private $maxAge;

    public function __construct(Age $minAge = null, Age $maxAge = null)
    {
        if ( ! $minAge && ! $maxAge) {
            throw new \InvalidArgumentException('Age range cannot be empty');
        }

        if ($minAge > $maxAge) {
            throw new \InvalidArgumentException(
                "Min age ($minAge) cannot be higher than max age ($maxAge)."
            );
        }

        $this->minAge = $minAge;
        $this->maxAge = $maxAge;
    }

    public static function build(array $ageRange): Specification
    {
        if ( ! $ageRange || \count($ageRange) > 2) {
            throw new \InvalidArgumentException('There must be 1 or 2 age values provided.');
        }

        $range[] = new Age($ageRange[0]);

        if (isset($ageRange[1])) {
            $range[] = new Age($ageRange[1]);
        }

        return new OrSpecification(...$range);
    }

    public static function fromString(string $minAge, string $maxAge): self
    {
        return new self(
            new Age((int) $minAge),
            new Age((int) $maxAge)
        );
    }

    public function minAge(): Age
    {
        return $this->minAge;
    }

    public function maxAge(): Age
    {
        return $this->maxAge;
    }

    public function __toString()
    {
        return $this->minAge . ' - ' . $this->maxAge;
    }

}