<?php

declare(strict_types=1);

namespace App\Domain\Model\Relations;

use App\Domain\Model\User\User;

final class Friendship
{
    private $userA;
    private $userB;
    private $established;

    private function __construct(User $userA, User $userB, \DateTimeImmutable $established)
    {
        $this->userA       = $userA;
        $this->userB       = $userB;
        $this->established = $established;
    }

    public static function make($user1, $user2): self
    {
        return new self($user1, $user2, new \DateTimeImmutable());
    }

    public function userA(): User
    {
        return $this->userA;
    }

    public function userB(): User
    {
        return $this->userB;
    }

    public function established(): \DateTimeImmutable
    {
        return $this->established;
    }

}
