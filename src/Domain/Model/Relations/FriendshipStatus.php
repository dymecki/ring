<?php

declare(strict_types = 1);

namespace App\Domain\Model\Relations;

use App\Domain\Model\User\User;

final class FriendshipStatus
{
    private const STATUS_FRIENDS   = 2;
    private const STATUS_PENDING   = 1;
    private const STATUS_STRANGERS = 0;

    private $userA;
    private $userB;
    private $status;

    private function __construct(User $userA, User $userB, int $status)
    {
        if ( ! \in_array($status, [
            self::STATUS_FRIENDS,
            self::STATUS_PENDING,
            self::STATUS_STRANGERS
        ], true)) {
            throw new \InvalidArgumentException("Invalid status: $status");
        }

        $this->userA  = $userA;
        $this->userB  = $userB;
        $this->status = $status;
    }

    public function userA(): User
    {
        return $this->userA;
    }

    public function userB(): User
    {
        return $this->userB;
    }

    public function status(): int
    {
        return $this->status;
    }

}
