<?php

declare(strict_types=1);

namespace App\UI\Web;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Twig\Extension\DebugExtension;

final class Render
{
    private $twig;

    public function __construct()
    {
        $loader     = new FilesystemLoader('../src/UI/Web/Templates');
        $this->twig = new Environment($loader, [
            'debug' => true
//            'cache' => '/path/to/compilation_cache',
        ]);

        $this->twig->addExtension(new DebugExtension());
    }

    public function get(string $template)
    {
        return $this->twig->render($template);
    }

}
