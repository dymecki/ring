<?php

declare(strict_types = 1);

namespace App\SearchUsers\Sets;

use App\Domain\Model\User\Properties\HairColor;

final class HairColorSet
{
    private $colors;

    public function __construct($colors)
    {
        $this->colors = $colors;
    }

    public function add(HairColor $color)
    {
        $this->colors[] = $color;
    }
}