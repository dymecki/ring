<?php

declare(strict_types = 1);

namespace App\SearchUsers\Sets;

use App\Domain\Model\User\Properties\Gender;

final class GenderSet
{
    private $genders;

    public function __construct($genders = [])
    {
        $this->genders = $genders;
    }

    public function add(Gender $gender): self
    {
        $this->genders[] = $gender;
        return $this;
    }
}