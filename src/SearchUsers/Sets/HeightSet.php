<?php

declare(strict_types = 1);

namespace App\SearchUsers\Sets;

use App\Domain\Model\User\Properties\Height;

final class HeightSet
{
    private $min;
    private $max;

    private function __construct(Height $min, Height $max)
    {
        if ($max < $min) {
            throw new \InvalidArgumentException(
                'Min height cannot be grater than max height.'
            );
        }

        $this->min = $min;
        $this->max = $max;
    }

    public static function build(int $min, int $max): self
    {
        $minHeight = new Height($min ?: Height::HEIGHT_MIN);
        $maxHeight = new Height($max ?: Height::HEIGHT_MAX);

        return new self($minHeight, $maxHeight);
    }

    public function min(): Height
    {
        return $this->min;
    }

    public function max(): Height
    {
        return $this->max;
    }

    public function range(): array
    {
        return range(
            $this->min()->value(),
            $this->max()->value()
        );
    }
}
