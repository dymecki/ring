<?php

declare(strict_types = 1);

namespace App\SearchUsers;

use App\SearchUsers\Parsers\HttpSearchParser;

final class SearchService
{
    private $properties;

    public function __construct(array $properties = [])
    {
        $this->properties = $properties;
    }

    public function find($request)
    {
        $parser = HttpSearchParser::factory($request);
    }


}