<?php

declare(strict_types = 1);

namespace App\SearchUsers\Mappers\PostgresArray;

use App\SearchUsers\Mappers\PropertyMapper;

final class AgeMapper implements PropertyMapper
{
    private const TAG_NAME = 'age';

//    public function map($property)
//    {
//        return array_map(function($age) {
//            return self::TAG_NAME . '_' . $age;
//        }, $this->data($property));
//    }

    public function data($property)
    {
        return array_flip($property);
    }
}