<?php

declare(strict_types = 1);

namespace App\SearchUsers\Mappers\PostgresArray;

use App\SearchUsers\Mappers\BasePropertyMapper;
use App\SearchUsers\Mappers\PropertyMapper;

final class GenderMapper extends BasePropertyMapper implements PropertyMapper
{
    protected const TAG_NAME = 'gender';

//    public function map($property)
//    {
//        return array_map(function($age) {
//            return self::TAG_NAME . '_' . $age;
//        }, $property);
//    }
}