<?php

declare(strict_types = 1);

namespace App\SearchUsers\Mappers;

abstract class BasePropertyMapper
{
    public function map($properties)
    {
        return implode(',', array_map(function($item) {
            return "'self::TAG_NAME_$item'";
        }, $properties));
    }
}