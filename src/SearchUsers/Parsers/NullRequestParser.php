<?php

declare(strict_types = 1);

namespace App\SearchUsers\Parsers;

use App\SearchUsers\Parsers\Http;

final class NullRequestParser implements RequestParser
{
    public static function build($property)
    {
    }
}