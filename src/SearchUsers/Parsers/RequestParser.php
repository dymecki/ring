<?php

declare(strict_types = 1);

namespace App\SearchUsers\Parsers;

interface RequestParser
{
    public static function build($age);
}