<?php

declare(strict_types = 1);

namespace App\SearchUsers\Parsers\Http;

use App\SearchUsers\Parsers\RequestParser;
use App\SearchUsers\Sets\AgeSet;

final class AgeParser implements RequestParser
{
    public static function build($age)
    {
        return AgeSet::build(
            (int) $age['min'],
            (int) $age['max']
        );
    }

}