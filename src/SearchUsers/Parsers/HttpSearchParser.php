<?php

declare(strict_types = 1);

namespace App\SearchUsers\Parsers;

use App\SearchUsers\Parsers\RequestParser;
use App\SearchUsers\Parsers\Http\AgeParser;

final class HttpSearchParser
{
    public static function factory($request): array
    {
//        return array_map(function($name, $item) {
//            return self::getParser($name)::build($item);
//        }, array_keys($request), $request);

        $entities = [];

        foreach ($request as $name => $item) {
            $entities[$name] = self::parser($name)::build($item);
        }

        return $entities;
    }

    private static function parser($formProperty)
    {
        $parser = null;

        switch ($formProperty) {
            case 'age':
                $parser = new AgeParser();
                break;
            case 'gender':
                $parser = new GenderParser();
                break;
            default:
                $parser = new NullRequestParser();
        }

        return $parser;
    }

}