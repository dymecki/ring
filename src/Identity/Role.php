<?php

declare(strict_types = 1);

namespace App\Identity;

final class Role
{
    private $id;
    private $name;

    public function __construct(int $id, string $name)
    {
        if ($id < 1) {
            throw new \InvalidArgumentException('Role\'s id cannot be smaller than zero');
        }

        if (strlen($name) < 2) {
            throw new \InvalidArgumentException('Role\'s name cannot be shorter than two characters');
        }

        $this->id   = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

}
