<?php

declare(strict_types = 1);

namespace App\Identity;

use App\Identity\User;
use App\Identity\UserBuilder;

final class BuilderDirector
{
    private $builder;

    public function __construct(UserBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function build(): User
    {
        return $this->builder->build();
    }

}
