<?php

declare(strict_types = 1);

namespace App\Identity;

use App\Identity\User;

interface UserBuilder
{
    public function build(): User;
    public function setId(): UserBuilder;
    public function setName(): UserBuilder;
    public function setRole(): UserBuilder;
}
