<?php

declare(strict_types = 1);

namespace App\Identity;

use App\Identity\User;
use App\Identity\UserBuilder;

final class SystemUserBuilder implements UserBuilder
{
    private $id;
    private $name;
    private $role;

    public function build(): User
    {
        return new User($this->id, $this->name, $this->role);
    }

    public function setId(): UserBuilder
    {
        $this->id = 1;
        return $this;
    }

    public function setName(): UserBuilder
    {
        $this->name = 'System';
        return $this;
    }

    public function setRole(): UserBuilder
    {
        $this->role = 1;
        return $this;
    }

}
