<?php

declare(strict_types = 1);

namespace App\Identity;

final class User
{
    private $id;
    private $name;
    private $role;

    public function __construct($id, $name, $role)
    {
        $this->id   = $id;
        $this->name = $name;
        $this->role = $role;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRole()
    {
        return $this->role;
    }

}
