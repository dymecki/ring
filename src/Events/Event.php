<?php

declare(strict_types = 1);

namespace App\Events;

interface Event
{
    public function appearedAt(): \DateTime;
//    public function name(): string;
}
