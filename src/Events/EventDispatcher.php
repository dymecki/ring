<?php

declare(strict_types = 1);

namespace App\Events;

final class EventDispatcher
{
    private $listeners = [];

    public function bind(string $eventName, EventListener $listener): void
    {
        $this->listeners[$eventName][] = $listener;
    }

    /** @return EventListener[] */
    public function registered(string $eventName): array
    {
        return $this->listeners[$eventName] ?? [];
    }

}
