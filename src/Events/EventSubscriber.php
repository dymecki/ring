<?php

declare(strict_types = 1);

namespace App\Events;

use App\Events\Event;

interface EventSubscriber
{
    public function handle(Event $event);
//    public function isSubscribedTo($event): bool;
}
