<?php

declare(strict_types = 1);

namespace App\Events;

use App\Events\Event;

interface EventListener
{
    public function handle(Event $event);
}
