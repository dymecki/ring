<?php

declare(strict_types = 1);

namespace App\Events;

abstract class BaseEvent implements Event
{
    protected $aggregateId;
    protected $appearedAt;

    public function __construct($aggregateId)
    {
        $this->aggregateId = $aggregateId;
        $this->appearedAt  = new \DateTime();
    }

    public function aggregateId()
    {
        return $this->aggregateId;
    }

    public function name(): string
    {
        return rtrim((new \ReflectionClass($this))->getShortName(), 'Event');
    }

    public function appearedAt(): \DateTime
    {
        return $this->appearedAt;
    }

}
