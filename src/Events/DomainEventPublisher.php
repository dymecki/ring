<?php

declare(strict_types = 1);

namespace App\Events;

final class DomainEventPublisher
{
    private $subscribers;
    private static $instance;

    private function __construct()
    {
        $this->subscribers = [];
    }

    public static function instance(): self
    {
        if (static::$instance == null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function subscribe(EventSubscriber $eventSubscriber): void
    {
        $this->subscribers = $eventSubscriber;
    }

    public function publish(Event $event): void
    {
        foreach ($this->subscribers as $subscriber) {
            $subscriber->handle($event);

//            if ($subscriber->isSubscribedTo($event)) {
//                $subscriber->handle($event);
//            }
        }
    }

}
