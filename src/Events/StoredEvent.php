<?php

declare(strict_types=1);

namespace App\Events;

final class StoredEvent implements Event
{
//    private $eventId;
    private $content;
    private $appearedAt;
    private $name;

    public function __construct(string $name, \DateTime $appearedAt, $content)
    {
        $this->name       = $name;
        $this->appearedAt = $appearedAt;
        $this->content    = $content;
    }

//    public function eventId()
//    {
//        return $this->eventId;
//    }

    public function name(): string
    {
        return $this->name;
    }

    public function appearedAt(): \DateTime
    {
        return $this->appearedAt;
    }

    public function content()
    {
        return $this->content;
    }

}
