<?php

declare(strict_types = 1);

namespace App\CommandBus;

final class SimpleCommandBus implements CommandBus
{
    private $handlers = [];

    public function registerHandler(string $commandClass, $handler): void
    {
        $this->handlers[$commandClass] = $handler;
    }

    public function handle(Command $command): void
    {
        $className = $this->handlerClass($command);

        $this->handlerFactory($className)->handle($command);
//        $this->handlers[get_class($command)]->handle($command);
    }

    public function handlerClass(Command $command): string
    {
        return get_class($command) . 'Handler';
    }

    public function handlerFactory(string $className): object
    {
        return new $className;
    }

}
