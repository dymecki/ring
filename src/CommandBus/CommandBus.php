<?php

declare(strict_types = 1);

namespace App\CommandBus;

interface CommandBus
{
    public function registerHandler(string $commandClass, $handler);
    public function handle(Command $command);
}
