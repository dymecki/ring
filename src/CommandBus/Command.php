<?php

declare(strict_types = 1);

namespace App\CommandBus;

interface Command
{
    public function commandName(): string;
    public function __toString();
}
