<?php

declare(strict_types = 1);

namespace App\CommandBus;

abstract class BaseCommand
{
    public function commandName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function __toString()
    {
        $result = '';

        foreach ((array) $this as $item) {
            $result .= is_array($item) ? implode(', ', $item) : $item . ', ';
        }

        return rtrim($result, ', ');
    }
}
