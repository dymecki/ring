<?php

declare(strict_types = 1);

namespace App\CommandBus\Commands\Admin;

use App\CommandBus\Handler;
use App\CommandBus\Command;
use App\Admin\Application\RunIndicator;

final class RunIndicatorCommandHandler implements Handler
{
    public function handle(Command $command): void
    {
        $runIndicator = new RunIndicator($command->getIndicatorSymbol());

        foreach ($command->getIndicatorParams() as $param) {
            $runIndicator->addParam((int) $param);
        }

        $runIndicator->run();
    }

}
