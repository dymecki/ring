<?php

declare(strict_types = 1);

namespace App\CommandBus\Commands\Admin;

use App\CommandBus\Command;
use App\CommandBus\BaseCommand;

final class RunIndicatorCommand extends BaseCommand implements Command
{
    private $indicatorSymbol;
    private $indicatorParams;

//    public function __construct(string $indicatorSymbol, array $indicatorParams)
//    {
//        $this->indicatorSymbol = $indicatorSymbol;
//        $this->indicatorParams = $indicatorParams;
//    }

    public function getIndicatorSymbol(): string
    {
        return $this->indicatorSymbol;
    }

    public function getIndicatorParams(): array
    {
        return $this->indicatorParams;
    }

}
