<?php

declare(strict_types = 1);

namespace App\CommandBus;

use App\CommandBus\CommandBus;
use App\CommandBus\SimpleCommandBus;

final class CommandBusFactory
{
    public static function build(): CommandBus
    {
        return new SimpleCommandBus();
    }
}
