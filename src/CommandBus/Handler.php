<?php

declare(strict_types = 1);

namespace App\CommandBus;

use App\CommandBus\Command;

interface Handler
{
    public function handle(Command $command): void;
}
