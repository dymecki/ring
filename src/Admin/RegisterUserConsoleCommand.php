<?php

declare(strict_types = 1);

namespace App\Admin;

use App\Application\Commands\RegisterUserCommand;
use App\System\System;
use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterUserConsoleCommand extends ConsoleCommand
{
    protected function configure()
    {
        $this->setName('user:register')
            ->setDescription('Register a new user in the system')
            ->addArgument('Name', InputArgument::REQUIRED)
            ->addArgument('Birthdate', InputArgument::REQUIRED)
            ->addArgument('Email', InputArgument::REQUIRED)
            ->addArgument('Password', InputArgument::REQUIRED)
            ->addArgument('PasswordConfirmation', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        System::run()->handle(
            new RegisterUserCommand(
                $input->getArgument('Name'),
                $input->getArgument('Birthdate'),
                $input->getArgument('Email'),
                $input->getArgument('Password'),
                $input->getArgument('PasswordConfirmation')
            )
        );

        $output->writeln('User registered');
    }

}
