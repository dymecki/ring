<?php

declare(strict_types = 1);

namespace App\System\Authentication;

interface Authentication
{
    public function login(string $login, string $password): bool;

    public function isLogged(): bool;

    public function logout(): void;
}
