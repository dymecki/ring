<?php

declare(strict_types = 1);

namespace App\System\Authentication;

use App\Application\Services\PasswordHash;
use App\Domain\Model\User\User;
use App\Persistence\Dao\UserDao;

final class WebAuthentication implements Authentication
{
    public function login(string $login, string $password): bool
    {
        $user = (new UserDao())->getByLogin($login);
        $auth = PasswordHash::verify($password, $user->passwordHash());

        if ($auth) {
            session_start();

            $_SESSION['user'] = $user;

            return true;
        }

        return false;
    }

    public function user(): ?User
    {
        return $_SESSION['user'] ?? null;
    }

    public function isLogged(): bool
    {
        session_start();
        return isset($_SESSION['user']);
    }

    public function isGuest(): bool
    {
        return ! $this->isLogged();
    }

    public function logout(): void
    {
        session_start();
        session_destroy();
        $_SESSION = [];
    }

}
