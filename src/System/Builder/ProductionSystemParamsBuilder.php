<?php

declare(strict_types = 1);

namespace App\System\Builder;

use App\CommandBus\CommandBusFactory;
use App\Persistence\Connection\Db;
use App\System\Authentication\WebAuthentication;
use App\System\Config\MyConfig;
use App\System\Data\DataFactory;
use App\System\Logger\Loggers;

final class ProductionSystemParamsBuilder implements SystemBuilder
{
    private $params;

    public function __construct()
    {
        $this->params = new SystemParams();
    }

    public function setSession(): void
    {
        session_start();
    }

    public function setAuth(): void
    {
        $this->params->setAuth(new WebAuthentication());
    }

    public function setConfig(): void
    {
        $this->params->setConfig(MyConfig::init());
    }

    public function setData(): void
    {
        $this->params->setData(DataFactory::build(Db::instance()));
    }

    public function setDb(): void
    {
        $this->params->setDb(Db::instance());
    }

    public function setCommandBus(): void
    {
        $this->params->setCommandBus(CommandBusFactory::build());
    }

    public function setLoggers(): void
    {
        $this->params->setLoggers(Loggers::init(Db::instance()));
    }

    public function params(): SystemParams
    {
        return $this->params;
    }

}