<?php

declare(strict_types = 1);

namespace App\System\Builder;

interface SystemBuilder
{
    public function setAuth(): void;

    public function setSession(): void;

    public function setConfig(): void;

    public function setData(): void;

    public function setDb(): void;

    public function setCommandBus(): void;

    public function setLoggers(): void;

    public function params(): SystemParams;
}