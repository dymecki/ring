<?php

declare(strict_types = 1);

namespace App\System\Builder;

final class SystemParamsDirector
{
    public static function build(SystemBuilder $builder): SystemParams
    {
        $builder->setSession();
        $builder->setAuth();
        $builder->setDb();
        $builder->setConfig();
        $builder->setLoggers();
        $builder->setData();
        $builder->setCommandBus();

        return $builder->params();
    }
}