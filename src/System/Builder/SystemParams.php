<?php

declare(strict_types = 1);

namespace App\System\Builder;

use App\CommandBus\CommandBus;
use App\Persistence\Connection\Db;
use App\System\Authentication\Authentication;
use App\System\Data\Data;
use App\System\Logger\Loggers;
use App\System\Session\Session;

final class SystemParams
{
    private $db;
    private $auth;
    private $loggers;
    private $config;
    private $commandBus;
    private $data;
    private $session;

    public function setDb($db): void
    {
        $this->db = $db;
    }

    public function setAuth(Authentication $auth): void
    {
        $this->auth = $auth;
    }

    public function setLoggers(Loggers $loggers): void
    {
        $this->loggers = $loggers;
    }

    public function setConfig($config): void
    {
        $this->config = $config;
    }

    public function setCommandBus(CommandBus $commandBus): void
    {
        $this->commandBus = $commandBus;
    }

    public function setData(Data $data): void
    {
        $this->data = $data;
    }

    public function setSession($session): void
    {
        $this->session = $session;
    }

    public function auth(): Authentication
    {
        return $this->auth;
    }

    public function db(): Db
    {
        return $this->db;
    }

    public function commandBus(): CommandBus
    {
        return $this->commandBus;
    }

    public function loggers(): Loggers
    {
        return $this->loggers;
    }

    public function config()
    {
        return $this->config;
    }

    public function data(): Data
    {
        return $this->data;
    }

    public function session(): Session
    {
        return $this->session;
    }

}
