<?php

declare(strict_types = 1);

namespace App\System\Logger;

final class SystemLoggerBuilder implements LoggerBuilder
{
    private $logger;

    public function __construct()
    {
        $this->logger = new Logger('system');
        $logFilePath  = __DIR__ . '/../log/php_errors.log';
    }

    public function addHandler($handler)
    {

    }

    public function addProcessor($processor)
    {

    }

    public function build()
    {
        return $this->logger;
    }
}
