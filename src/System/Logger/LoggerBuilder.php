<?php

declare(strict_types = 1);

namespace App\System\Logger;

interface LoggerBuilder
{
    public function addHandler($handler);
    public function addProcessor($processor);
}
