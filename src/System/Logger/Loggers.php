<?php

declare(strict_types = 1);

namespace App\System\Logger;

use App\System\BaseSystemMember;
use App\System\SystemMember;
use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

final class Loggers extends BaseSystemMember implements SystemMember
{
    const LEVEL = \Monolog\Logger::DEBUG;

    private $db;
    private $systemLogger;
    private $commandLogger;
//    private $sqlLogger;

    private function __construct()
    {
    }

    public static function init($db): self
    {
        $self = new self();
        $self->setDb($db);
        $self->setSystemLogger();
//        $self->setSqlLogger();
        $self->setCommandLogger();

        return $self;
    }

    public function setDb($db)
    {
        $this->db = $db;
    }

    public function systemLogger(): LoggerInterface
    {
        return $this->systemLogger;
    }

    public function commandLogger(): LoggerInterface
    {
        return $this->commandLogger;
    }

//    public function sqlLogger(): LoggerInterface
//    {
//        return $this->sqlLogger;
//    }

    private function setSystemLogger(): self
    {
        $this->systemLogger = new Logger('system');
        $logFilePath        = __DIR__ . '/../log/php_errors.log';

        $handlerFile     = new StreamHandler($logFilePath, self::LEVEL);
        $handlerSql      = new PostgreSqlMonologHandler($this->pdo(), self::LEVEL);
        $handlerTerminal = new StreamHandler('php://stdout', self::LEVEL);

        $lineFormat = implode("\n", [
            "[%datetime%] [%channel%.%level_name%] =================",
            "%message%\n",
            "%context%",
            "%extra%\n"
        ]);

        $lineFormatter = new LineFormatter($lineFormat, 'd.m.Y H:i:s');
        $lineFormatter->includeStacktraces();

        $handlerFile->setFormatter($lineFormatter);
        $handlerTerminal->setFormatter($lineFormatter);

        $this->systemLogger()
            ->pushHandler($handlerFile)
            ->pushHandler($handlerSql)
            ->pushHandler($handlerTerminal)
            ->pushProcessor(new \Monolog\Processor\WebProcessor())
            ->pushProcessor(new \Monolog\Processor\MemoryUsageProcessor())
            ->pushProcessor(new \Monolog\Processor\ProcessIdProcessor())
            ->pushProcessor(new \JK\Monolog\Processor\RequestHeaderProcessor());
//            ->pushProcessor(new SqlQueryMonologProcessor($this->pdo()));

        ErrorHandler::register($this->systemLogger);

        return $this;
    }

    private function setCommandLogger(): self
    {
        $this->commandLogger = new Logger('command');
        $handlerSql          = new PostgreSqlMonologHandler($this->pdo(), self::LEVEL);

        $this->commandLogger()
            ->pushHandler($handlerSql)
            ->pushProcessor(new \Monolog\Processor\WebProcessor())
            ->pushProcessor(new \Monolog\Processor\MemoryUsageProcessor())
            ->pushProcessor(new \Monolog\Processor\ProcessIdProcessor())
            ->pushProcessor(new \JK\Monolog\Processor\RequestHeaderProcessor())
            ->pushProcessor(new UserIdMonologProcessor(7));

        return $this;
    }

//    private function setSqlLogger(): self
//    {
//        $this->sqlLogger = new Logger('sql');
//        $handlerSql      = new PostgreSqlMonologHandler($this->pdo(), self::LEVEL);
//
//        $this->sqlLogger()
//            ->pushHandler($handlerSql)
//            ->pushProcessor(new \Monolog\Processor\WebProcessor())
//            ->pushProcessor(new \Monolog\Processor\MemoryUsageProcessor())
//            ->pushProcessor(new \Monolog\Processor\ProcessIdProcessor())
//            ->pushProcessor(new \JK\Monolog\Processor\RequestHeaderProcessor())
//            ->pushProcessor(new UserIdMonologProcessor(7));
////            ->pushProcessor(new SqlQueryMonologProcessor($this->pdo()));
//
//        return $this;
//    }

    private function pdo()
    {
        return $this->db;
    }

}
