<?php

declare(strict_types = 1);

namespace App\System\Logger;

use Monolog\Handler\AbstractProcessingHandler;
use PdoChaining\Php7\PDO;

final class PostgreSqlMonologHandler extends AbstractProcessingHandler
{
    private $pdo;

    public function __construct($pdo, $debugLevel, bool $bubble = true)
    {
        $this->pdo = $pdo;

        parent::__construct($debugLevel, $bubble);
    }

    protected function write(array $record): void
    {
        $record = (object) $record;

        $stmt = $this->pdo->prepare('
            INSERT INTO system.logs (channel,
                                     message,
                                     level_name,
                                     context,
                                     user_id,
                                     ip,
                                     process_id,
                                     memory_usage,
                                     extra,
                                     request_headers)
            VALUES (:channel,
                    :message,
                    :level_name,
                    :context,
                    :user_id,
                    :ip,
                    :process_id,
                    :memory_usage,
                    :extra,
                    :request_headers)
        ');

        $context = $record->channel == 'command'
            ? $record->context[0]
            : json_encode((object) $record->context);

        $stmt->execute([
            'channel'         => $record->channel,
            'message'         => $record->message,
            'level_name'      => $record->level_name,
            'context'         => $context,
            'user_id'         => $record->extra['user_id'] ?? 1,
            'ip'              => $record->extra['ip'] ?? null,
            'process_id'      => $record->extra['process_id'],
            'memory_usage'    => $record->extra['memory_usage'],
            'extra'           => json_encode($record->extra),
            'request_headers' => isset($record->extra['headers']) ? json_encode($record->extra['headers']) : null,
        ]);
    }

}
