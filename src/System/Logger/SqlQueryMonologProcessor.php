<?php

declare(strict_types = 1);

namespace App\System\Logger;

class SqlQueryMonologProcessor
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function __invoke(array $record): array
    {
        $record['extra']['user_id'] = $this->pdo->getLastQuery();

        return $record;
    }

}
