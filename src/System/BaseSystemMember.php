<?php

declare(strict_types = 1);

namespace App\System;

use App\System\System;

abstract class BaseSystemMember
{
    protected $system;

    public function addToSystem(System $system)
    {
        $this->system = $system;
    }

    protected function system(): System
    {
        return $this->system;
    }

}
