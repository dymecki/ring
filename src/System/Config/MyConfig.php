<?php

declare(strict_types = 1);

namespace App\System\Config;

use App\System\BaseSystemMember;
use App\System\SystemMember;
use Zend\Config\Config;
use Zend\Config\Factory;

final class MyConfig extends BaseSystemMember implements SystemMember
{
    private $config;

    private function __construct(Config $config)
    {
        $this->config = $config;
    }

    public static function init()
    {
        return Factory::fromFile('/home/vagrant/code/PHP/Trader/App/config/config.ini', true);

//        echo __DIR__."\n";exit;
//        return new self(
//            Factory::fromFile('/home/vagrant/code/PHP/Trader/App/config/config.ini', true)
////            Factory::fromFile('../../../App/config/config.ini', true)
//        );
    }

    public function get(string $item)
    {

    }

}
