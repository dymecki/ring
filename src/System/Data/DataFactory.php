<?php

declare(strict_types = 1);

namespace App\System\Data;

use App\Persistence\Connection\Db;

final class DataFactory
{
    public static function build(Db $db): Data
    {
        $dataBuilder = null;

//        if ($db instanceof \PdoChaining\Php7\PDO) {
            $dataBuilder = new DaoDataBuilder($db);
//        }

        if (!$dataBuilder) {
            throw new \Exception('Wrong database driver');
        }

        return $dataBuilder->setRepositories()->build();
    }

}
