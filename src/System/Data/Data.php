<?php

declare(strict_types = 1);

namespace App\System\Data;

use App\Persistence\Dao\BaseDao;
use App\System\SystemMember;
use App\System\BaseSystemMember;

final class Data extends BaseSystemMember implements \Countable, SystemMember
{
    private $repositories = [];

    public function __call(string $methodName, $args = null): BaseDao
    {
        if (!$this->respositoryExists($methodName)) {
            throw new \InvalidArgumentException("No such repository: $methodName");
        }

        return $this->repositories[$methodName];
    }

    public function addRepository(BaseDao $repository): self
    {
        $this->repositories[$repository->name()] = $repository;
        return $this;
    }

    private function respositoryExists(string $name): bool
    {
        return isset($this->repositories[$name]);
    }

    public function count(): int
    {
        return count($this->repositories);
    }

}
