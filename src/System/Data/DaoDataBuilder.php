<?php

declare(strict_types = 1);

namespace App\System\Data;

use App\Persistence\Dao\AssetDao;
use App\Persistence\Dao\AssetIndicatorCombinationDao;
use App\Persistence\Dao\CandlesDao;
use App\Persistence\Dao\DataProviderDao;
use App\Persistence\Dao\IndicatorCalculationDao;
use App\Persistence\Dao\IndicatorCombinationDao;
use App\Persistence\Dao\IndicatorDao;
use App\Persistence\Dao\IndicatorParameterDao;
use App\Persistence\Dao\MarketDao;
use App\Persistence\Dao\QuoteDao;
use App\Persistence\Dao\ScheduleDao;
use App\Persistence\Dao\SignalDao;

final class DaoDataBuilder
{
    private $data;
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo  = $pdo;
        $this->data = new Data();
    }

    public function setRepositories(): self
    {
        $pdo = $this->pdo;

//        $this->data
//            ->addRepository(new AssetIndicatorCombinationDao($pdo))
//            ->addRepository(new AssetDao($pdo))
//            ->addRepository(new CandlesDao($pdo))
//            ->addRepository(new DataProviderDao($pdo))
//            ->addRepository(new IndicatorCalculationDao($pdo))
//            ->addRepository(new IndicatorCombinationDao($pdo))
//            ->addRepository(new IndicatorParameterDao($pdo))
//            ->addRepository(new IndicatorDao($pdo))
//            ->addRepository(new MarketDao($pdo))
//            ->addRepository(new QuoteDao($pdo))
//            ->addRepository(new ScheduleDao($pdo))
//            ->addRepository(new SignalDao($pdo))
//            ->addRepository(new PermissionsDao($pdo))
//            ->addRepository(new RolesDao($pdo))
//            ->addRepository(new UsersDao($pdo));

        return $this;
    }

    public function build(): Data
    {
        return $this->data;
    }

}
