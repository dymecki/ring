<?php

declare(strict_types = 1);

namespace App\System\Session;

use App\Domain\Model\User\User;

final class Session
{
    public function user(): User
    {
        return $_SESSION['user'];
    }
}
