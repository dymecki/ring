<?php

declare(strict_types = 1);

namespace App\System;

use App\CommandBus\Command;
use App\CommandBus\CommandBus;
use App\Domain\Model\User\User;
use App\Persistence\Connection\Db;
use App\System\Authentication\Authentication;
use App\System\Builder\ProductionSystemParamsBuilder;
use App\System\Builder\SystemParams;
use App\System\Builder\SystemParamsDirector;
use App\System\Data\Data;
use App\System\Logger\Loggers;
use App\System\Session\Session;

final class System
{
    private static $instance;
    private $params;

    private function __construct(SystemParams $systemParams)
    {
        $this->params = $systemParams;
    }

    public static function run(): self
    {
        if ( ! self::$instance) {
            self::$instance = new self(
                SystemParamsDirector::build(new ProductionSystemParamsBuilder())
            );
        }

        return self::$instance;
    }

    public function handle(Command $command): void
    {
        $commandName   = $command->commandName();
        $commandParams = [(string) $command];

        $this->loggers()->commandLogger()->info($commandName, $commandParams);
        $this->commandBus()->handle($command);
    }

    public function query($query)
    {

    }

    public function user(): User
    {
        return $this->params->session()->user();
    }

    public function auth(): Authentication
    {
        return $this->params->auth();
    }

    public function session(): Session
    {
        return $this->params->session();
    }

    public function db(): Db
    {
        return $this->params->db();
    }

    public function loggers(): Loggers
    {
        return $this->params->loggers();
    }

    public function commandBus(): CommandBus
    {
        return $this->params->commandBus();
    }

    public function config()
    {
        return $this->params->config();
    }

    public function data(): Data
    {
        return $this->params->data();
    }

}
