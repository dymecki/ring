<?php

declare(strict_types = 1);

namespace App\System;

interface SystemMember
{
    public function addToSystem(System $system);
}
