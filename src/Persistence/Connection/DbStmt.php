<?php

declare(strict_types = 1);

namespace App\Persistence\Connection;

final class DbStmt
{
    private $stmt;

    public function __construct(\PDOStatement $stmt)
    {
        $this->stmt = $stmt;
    }

    public function execute(array $params = null): \PDOStatement
    {
        $this->stmt->execute($params);
        return $this->stmt;
    }
}