<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

use App\Domain\Model\Conversation\Message;
use App\Domain\Model\User\User;

final class ConversationDao extends BaseDao
{
    public function add($senderId, $receiverId, $message): void
    {
        $this->db()
             ->prepare('
                insert into messages.messages (sender_id, receiver_id, message)
                     values (:sender_id, :receiver_id, :message)
            ')
             ->execute([
                           'sender_id'   => $senderId,
                           'receiver_id' => $receiverId,
                           'message'     => $message
                       ]);
    }

    public function getConversation($me, $friend): array
    {
        return $this
            ->db()
            ->prepare("
                select *
                  from (
                    select m.*,
                           to_char(m.created_at, 'YYYY-MM-DD HH24:MI') datetime,
                           friend.id user_id,
                           friend.name user_name,
                           p.path user_photo,
                           'user-a' user_type
                      from messages.messages m
                           join users.users friend
                             on friend.id = m.sender_id
                           join resources.photos p
                             on p.user_id = friend.id
                     where receiver_id = :me and sender_id = :friend
    
                     union all
    
                    select m.*,
                           to_char(m.created_at, 'YYYY-MM-DD HH24:MI') datetime,
                           me.id user_id,
                           me.name user_name,
                           p.path user_photo,
                           'user-b' user_type
                      from messages.messages m
                           join users.users me
                             on me.id = m.sender_id
                           join resources.photos p
                             on p.user_id = me.id
                     where receiver_id = :friend and sender_id = :me
                ) messages                   
                order by messages.datetime desc
             ")
            ->execute(['me' => $me, 'friend' => $friend])
            ->fetchAll();
    }

    public function getConversations(User $userA): array
    {
        return $this
            ->db()
            ->prepare("
                select m.receiver_id friend_id,
                       bool_or(m.read) msg_read,
                       to_char(max(m.created_at), 'HH24:MI DD-MM-YYYY') last_message_date,
                       u.name friend_name,
                       count(*) amount,
                       p.path avatar
                  from messages.messages m
                       join users.users u
                         on u.id = m.receiver_id
                       join resources.photos p
                         on p.user_id = u.id
                 where sender_id = :user or receiver_id = :user
                 group by m.receiver_id, u.name, p.path
            ")
            ->execute(['user' => $userA->id()])
            ->fetchAll();
    }

    public function getAllBySender(User $sender): array
    {
        return $this
            ->db()
            ->prepare('
                select *
                  from messages.messages m
                       join users.users u
                         on m.sender_id = u.id
                 where sender_id = ?
                 order by m.created_at asc
            ')
            ->execute([$sender->id()])
            ->fetchAll();
    }

    public function getAllByReceiver(User $sender): array
    {
        return $this
            ->db()
            ->prepare('
                select *
                  from messages.messages m
                       join users.users u
                         on m.receiver_id = u.id
                 where receiver_id = ?
                 order by m.created_at asc
             ')
            ->execute([$sender->id()])
            ->fetchAll();
    }

    public function getById(string $id)
    {
        return $this
            ->db()
            ->prepare('select * from messages.messages where id = ? limit 1')
            ->execute([$id])
            ->fetch();
    }

}
