<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

use App\Domain\Model\User\User;
use App\Domain\Model\User\Email;

final class UserDao extends BaseDao
{
    //    public function all()
    //    {
    //        return $this->db()->query('select * from users.users')->fetchall();
    //    }
    //
    //    public function allFull()
    //    {
    //        return $this->db()->run(__FUNCTION__)->fetchAll();
    //    }

    public function getById($id): User
    {
        $result = $this
            ->db()
            ->prepare('select * from users.users where id = ? limit 1')
            ->execute([$id])
            ->fetch();

        return User::init(
            $result->id,
            $result->name,
            $result->email,
            $result->password_hash,
            $result->created_at
        );
    }

    public function userExists(Email $email): bool
    {
        return (bool) $this
            ->db()
            ->prepare('select 1 from users.users where email = ? limit 1')
            ->execute([(string) $email])
            ->fetch()
            ->count;
    }

    public function add($name, $login, $password): void
    {
        $this->db()
             ->prepare('insert into users.users (name, email, password_hash) values (?, ?, ?)')
             ->execute([(string) $name, (string) $login, (string) $password]);
    }

    public function getByLogin(string $login): User
    {
        $result = $this
            ->db()
            ->prepare('select * from users.users where name = ? limit 1')
            ->execute([$login])
            ->fetch();

        return User::init(
            $result->id,
            $result->name,
            $result->email,
            $result->password_hash,
            $result->created_at
        );
    }

}
