<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

use App\Domain\Model\User\User;
use App\Domain\Model\User\Email;

final class LoginDao extends BaseDao
{
    //    public function all()
    //    {
    //        return $this->db()->query('select * from users.logins')->fetchall();
    //    }

    public function userExists(Email $email): bool
    {
        return (bool) $this
            ->db()
            ->prepare('select 1 from users.users where email = ? limit 1')
            ->execute([(string) $email])
            ->fetch()
            ->count;
    }

    //    public function add($name, $login, $password)
    //    {
    //        $this->db()->run(__FUNCTION__, [
    //            (string) $name,
    //            (string) $login,
    //            (string) $password
    //        ]);
    //    }

    public function auth($login, $password)
    {
        return $this
            ->db()
            ->prepare('select * from users.users where name = ? and password_hash = ? limit 1')
            ->execute([$login, $password])
            ->fetch();
    }

}
