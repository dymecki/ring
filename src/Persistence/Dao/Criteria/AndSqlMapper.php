<?php

declare(strict_types = 1);

namespace App\Persistence\Dao\Criteria;

use App\Domain\Model\Search\Criteria\Criteria;
use App\Domain\Model\Search\Specification\AndSpecification;

final class AndSqlMapper
{
    private $specification;
    private $columns = [];

    public function __construct(AndSpecification $specification)
    {
        $this->specification = $specification;
        $this->columns       = [
            'Language' => 'users.profiles.properties->language'
        ];
    }

    public function code()
    {
        return join(' AND ', $this->specification->specifications());

        $result = '';

        foreach ($this->specification->specifications() as $specification) {
        }
    }

}