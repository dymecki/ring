<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

use App\Domain\Model\User\User;

final class FriendsDao extends BaseDao
{
    public function checkIfFriends(string $hash)
    {
        $result = $this
            ->db()
            ->prepare('select status from users.friends where users_hash = ? limit 1')
            ->execute([$hash]);

        return $result ?? false;
    }

    //    public function checkIfFriends(User $me, $friendId)
    //    {
    //        $this->db()->prepare('
    //            SELECT *
    //              FROM users.friends f
    //             WHERE (user_id = :me AND friend_id = :friend)
    //                OR (user_id = :friend AND friend_id = :me)
    //             LIMIT 1
    //        ')->execute(['me' => $me->id(), 'friend' => $friendId]);
    //    }
    //
    public function add(string $userId, string $friendId): void
    {
        $this->db()
             ->prepare('insert into users.friends (user_id, friend_id) values (?, ?)')
             ->execute([$userId, $friendId]);
    }

    public function remove(string $userId, string $friendId): void
    {
        $this->db()
             ->prepare('
                delete from users.friends
                 where (user_id = :user_id and friend_id = :friend_id)
                    or (user_id = :friend_id and friend_id = :user_id)
            ')
             ->execute([
                           'user_id'   => $userId,
                           'friend_id' => $friendId
                       ]);
    }

    public function getUserFriends(string $userId): array
    {
        return $this
            ->db()
            ->prepare("
                select f2.*,
                       u.name,
                       p.path filename
                  from (select case
                                    when user_id = :user_id
                                    then friend_id
                                    else user_id 
                                end friend_id,
                               to_char(created_at, 'dd.mm.yyyy') as created_at
                          from users.friends
                         where user_id = :user_id or friend_id = :user_id
                     ) f2
                       join users.users u
                         on u.id = friend_id
                       join resources.photos p
                         on p.user_id = friend_id
            ")
            ->execute(['user_id' => $userId])
            ->fetchAll();
    }

}
