<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

final class UserSearchDao extends BaseDao
{
    public function search(array $tags, int $limit = 50): array
    {
        //        var_dump($tags);
        //        exit;
        return $this
            ->db()
            ->prepare('
                select up.user_id,
                       up.name,
                       date_part(\'year\', age(up.birthdate)) age,
                       rp.path
                  from users.profiles up
                        join resources.photos rp
                          on rp.user_id = up.user_id 
                 where up.properties && :gender 
                   and up.properties && :age
                 limit :limit
            ')
            ->execute([
                          'gender' => $tags['gender'],
                          'age'    => $tags['age'],
                          'limit'  => $limit
                      ])
            ->fetchAll();
    }

}
