<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

use App\Domain\Model\User\Profile\Profile;

final class ProfileDao extends BaseDao
{
    public function getByUserId(string $userId): Profile
    {
        $result = $this
            ->db()
            ->prepare('
                select p.*,
                       date_part(\'year\', age(p.birthdate)) age,
                       rp.path photo_id,
                       rp.width,
                       rp.height
                  from users.profiles p
                       join resources.photos rp using(user_id)
                 where p.user_id = ?
                 limit 1
            ')
            ->execute([$userId])
            ->fetch();

        return Profile::init($result->age, $result->name, $result->properties);
    }

    public function add($userId, $properties): void
    {
        $this->db()
             ->prepare('insert into users.profiles (user_id, properties) values (?, ?)')
             ->execute([$userId, $properties]);
    }

    public function validate(string $data): bool
    {
        return (bool) $this
            ->db()
            ->prepare('select 1 from dictionaries.profile where properties @> ? limit 1')
            ->execute([$data])
            ->fetch();
    }

}
