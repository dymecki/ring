<?php

declare(strict_types = 1);

namespace App\Persistence\Dao\Users;

use App\Persistence\Dao\BaseDao;

final class UsersDao extends BaseDao
{
    public function getAll()
    {
        return $this->db()->query('
            select u.*,
                   r.name
              from users.users u
                   join users.users_roles ur    on ur.user_id = u.id
                   join users.roles r           on ur.role_id = r.id
        ')->fetchAll();
    }

    public function getById(int $userId)
    {
        return $this->db()->prepare('
            select u.*,
                   r.name
              from users.users u
                   join users.users_roles ur    on ur.user_id = u.id
                   join users.roles r           on ur.role_id = r.id
             where u.id = ?
             limit 1
        ')->execute([$userId])->fetch();
    }

}
