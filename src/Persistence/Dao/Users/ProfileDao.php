<?php

declare(strict_types = 1);

namespace App\Persistence\Dao\Users;

use App\Persistence\Dao\BaseDao;
use App\Domain\Model\User\User;

final class ProfileDao extends BaseDao
{
    public function getById(User $user)
    {
        return $this
            ->db()
            ->prepare('select * from users.profiles where user_id = ? limit 1')
            ->execute([$user->id()])
            ->fetch();
    }

}
