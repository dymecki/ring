<?php

declare(strict_types = 1);

namespace App\Persistence\Dao\Users;

use App\Persistence\Dao\BaseDao;

final class RolesDao extends BaseDao
{
    public function getAll()
    {
        return $this->db()->query('select * from users.roles')->fetchAll();
    }

    public function getById(int $id)
    {
        return $this
            ->db()
            ->prepare('select * from users.roles where id = ? limit 1')
            ->execute([$id])
            ->fetch();
    }

}
