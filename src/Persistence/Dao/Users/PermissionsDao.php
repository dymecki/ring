<?php

declare(strict_types = 1);

namespace App\Persistence\Dao\Users;

use App\Persistence\Dao\BaseDao;

final class PermissionsDao extends BaseDao
{
    public function getAll()
    {
        return $this->db()->query('select * from users.permissions')->fetchAll();
    }

    public function getById(int $id)
    {
        return $this
            ->db()
            ->prepare('select * from users.permissions where id = ? limit 1')
            ->execute([$id])
            ->fetch();
    }

}
