<?php

declare(strict_types = 1);

namespace App\Persistence\Dao;

final class LogCommandDao extends BaseDao
{
    public function add(string $command, string $params, int $ownerId)
    {
        $this->db()
             ->prepare('insert into logs.commands (command, params, owner_id) values (?, ?, ?)')
             ->execute([$command, $params, $ownerId]);
    }

    public function getByUserId(int $userId, int $limit = null)
    {
        return $this
            ->db()
            ->prepare('select * from logs.commands where owner_id = ? order by created_at desc')
            ->execute([$userId])
            ->fetchAll();
    }

    public function getLastCommands(int $commandsAmount)
    {
        return $this
            ->db()
            ->prepare('select * from logs.commands order by created_at desc limit ?')
            ->execute([$commandsAmount])
            ->fetchAll();
    }

}
