<?php

declare(strict_types = 1);

namespace App\Application;

use App\Domain\Model\User\User;

final class PublishPhoto
{
    private $user;
    private $image;

    function __construct(User $user, Image $image)
    {
        $this->user  = $user;
        $this->image = $image;
    }

    public function publish(): void
    {

    }

}
