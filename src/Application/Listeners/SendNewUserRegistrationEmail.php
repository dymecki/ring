<?php

declare(strict_types=1);

namespace App\Application\Listeners;

use App\Domain\Model\User\Events\UserRegistered;

final class SendNewUserRegistrationEmail
{
    private $event;

    public function __construct(UserRegistered $event)
    {
        $this->event = $event;
    }

    public function send()
    {
        
    }

}
