<?php

declare(strict_types = 1);

namespace App\Application\Commands;

use App\CommandBus\Command;
use App\CommandBus\BaseCommand;
use App\Application\Commands\SearchUsersCommand;
use App\Application\Services\UserManager;
use App\Domain\Model\User\UserName;
use App\Domain\Model\User\UserPassword;
use App\Domain\Model\User\Email;

final class SearchUsersCommandHandler extends BaseCommand implements Command
{
    public function handle(SearchUsersCommand $command): void
    {
//        $userManager = new UserManager();
//
//        $userManager->register(
//            new UserName($command->getName()),
//            new Email($command->getEmail()),
//            new UserPassword($command->getPassword()),
//            new UserPassword($command->getPasswordConfirmation())
//        );
    }
}
