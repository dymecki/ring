<?php

declare(strict_types=1);

namespace App\Application\Commands;

use App\CommandBus\Command;
use App\CommandBus\BaseCommand;

final class LoginCommand extends BaseCommand implements Command
{
    private $login;
    private $password;

    public function __construct(string $login, string $password)
    {
        $this->login    = $login;
        $this->password = $password;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

}
