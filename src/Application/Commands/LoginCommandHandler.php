<?php

declare(strict_types = 1);

namespace App\Application\Commands;

use App\CommandBus\BaseCommand;
use App\CommandBus\Command;
use App\System\Authentication\WebAuthentication;

final class LoginCommandHandler extends BaseCommand implements Command
{
    public function handle(LoginCommand $command): void
    {
        $auth   = new WebAuthentication();
        $logged = $auth->login($command->getLogin(), $command->getPassword());
    }

}
