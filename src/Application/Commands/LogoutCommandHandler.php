<?php

declare(strict_types=1);

namespace App\Application\Commands;

use App\CommandBus\Command;
use App\CommandBus\BaseCommand;
use App\Application\Commands\LogoutCommand;
use App\Authentication\WebAuthentication;

final class LogoutCommandHandler extends BaseCommand implements Command
{
    public function handle(LogoutCommand $command): void
    {
        (new WebAuthentication())->logout();
    }

}
