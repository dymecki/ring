<?php

declare(strict_types=1);

namespace App\Application\Commands;

use App\Application\Services\MessageService;
use App\CommandBus\BaseCommand;
use App\CommandBus\Command;
use App\Domain\Model\Conversation\Message;

final class WriteMessageCommandHandler extends BaseCommand implements Command
{
    public function handle(WriteMessageCommand $command): void
    {
        $message = Message::write(
            $command->getSenderId(),
            $command->getReceiverId(),
            $command->getMessage(),
            (new \DateTime())->format('Y-m-d H:i:s')
        );

        (new MessageService())->send($message);
    }

}
