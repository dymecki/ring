<?php

declare(strict_types=1);

namespace App\Application\Commands;

use App\CommandBus\Command;
use App\CommandBus\BaseCommand;

final class RegisterUserCommand extends BaseCommand implements Command
{
    private $name;
    private $birthDate;
    private $email;
    private $password;
    private $passwordConfirmation;

    public function __construct(
        string $name,
        string $birthDate,
        string $email,
        string $password,
        string $passwordConfirmation
    )
    {
        $this->name                 = $name;
        $this->birthDate            = $birthDate;
        $this->email                = $email;
        $this->password             = $password;
        $this->passwordConfirmation = $passwordConfirmation;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBirthDate()
    {
        return $this->birthDate;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getPasswordConfirmation(): string
    {
        return $this->passwordConfirmation;
    }

}
