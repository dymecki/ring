<?php

declare(strict_types=1);

namespace App\Application\Commands;

use App\CommandBus\BaseCommand;
use App\CommandBus\Command;

final class WriteMessageCommand extends BaseCommand implements Command
{
    private $senderId;
    private $receiverId;
    private $message;

    public function __construct(string $senderId, string $receiverId, string $message)
    {
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->message    = $message;
    }

    public function getSenderId(): string
    {
        return $this->senderId;
    }

    public function getReceiverId(): string
    {
        return $this->receiverId;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

}
