<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Domain\Model\Conversation\Conversation;
use App\Domain\Model\User\UserId;
use App\Persistence\Dao\ConversationDao;
use App\Persistence\Dao\UserDao;
use App\System\Authentication\WebAuthentication;

final class MessageService
{
    private $messages;
    private $users;

    public function __construct($messages = null)
    {
        $this->messages = new ConversationDao();
        $this->users    = new UserDao();
    }

    public function write(string $senderId, string $receiverId, string $message): void
    {
        $this->messages->add($senderId, $receiverId, $message);
    }

    public function get(string $messageId)
    {
        return $this->messages->getById($messageId);
    }

    public function getList($user = null)
    {
        //        $received = $this->messages->getAllByReceiver((new WebAuthentication())->user());
        //        $sent     = $this->messages->getAllBySender((new WebAuthentication())->user());
        $result = $this->messages->getConversations((new WebAuthentication())->user());

        //        $result = array_merge($received, $sent);
        //        var_dump($result);
        //        exit;
        return $result;

        return array_merge($received, $sent);
    }

    public function conversation(UserId $me, UserId $friendId): Conversation
    {
        $friend   = $this->users->getById((string) $friendId);
        $messages = $this->messages->getConversation((string) $me, (string) $friendId);

        return new Conversation($me, $friend, $messages);
    }

}
