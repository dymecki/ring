<?php

declare(strict_types = 1);

namespace App\Application\Services\Criteria\Mappers;

final class OrSqlMapper
{
    private $properties;

    public function __construct($properties)
    {
        $this->properties = $properties;
    }

    public function map(): string
    {
        return implode(' OR ', $this->properties);
    }

    public function __toString()
    {
        return $this->map();
    }
}