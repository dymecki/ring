<?php

declare(strict_types = 1);

namespace App\Application\Services\Criteria\Mappers;

final class HairColorSqlMapper
{
    private $property;

    public function __construct($property)
    {
        $this->property = $property;
    }

    public function map(): string
    {
        return 'hair_color = ' . $this->property;
    }

    public function __toString()
    {
        return $this->map();
    }
}