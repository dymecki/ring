<?php

declare(strict_types = 1);

namespace App\Application\Services\Criteria\Mappers;

final class GenderSqlMapper
{
    private $gender;

    public function __construct($gender)
    {
        $this->gender = $gender;
    }

    public function map(): string
    {
        return 'gender = ' . $this->gender;
    }

    public function __toString()
    {
        return $this->map();
    }
}