<?php

declare(strict_types = 1);

namespace App\Application\Services;

final class SerializeService
{
    public static function serialize($data): string
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    public static function deserialize(string $serializedData): array
    {
        return (array) json_decode($serializedData);
    }
}