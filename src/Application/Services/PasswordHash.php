<?php

declare(strict_types=1);

namespace App\Application\Services;

final class PasswordHash
{
    public static function hash(string $password): string
    {
        // check if $password is not NULL byte
        if ($password) {
            // TODO
        }

        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
