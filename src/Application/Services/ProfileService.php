<?php

declare(strict_types = 1);

namespace App\Application\Services;

use App\Domain\Model\User\Profile\Profile;
use App\Domain\Model\User\User;
use App\Persistence\Dao\ProfileDao;

final class ProfileService
{
    private const DEFAULT_PROFILE = [
        'gender'     => 'Woman',
        'hair_color' => 'Brown',
        'language'   => 'english'
    ];

    private $profileDao;
    private $jsonProfileData;

    public function __construct()
    {
        $this->profileDao      = new ProfileDao();
        $this->jsonProfileData = SerializeService::serialize(self::DEFAULT_PROFILE);
    }

    public function createProfile($userId = null): void
    {
        $userId = $_SESSION['user']->id();

        if ( ! $this->validate($this->jsonProfileData)) {
            throw new \InvalidArgumentException('Invalid profile properties');
        }

        $this->profileDao->add($userId, $this->jsonProfileData);
    }

    public function queryBuilder($params)
    {
        foreach ($params as $key => $values) {
            if ($key === 'language') {
            }
        }
    }

    public function search(string $selector)
    {
        return $this->profileDao->search($selector);
    }

    public function validate(string $profileData): bool
    {
        return $this->profileDao->validate($profileData);
    }

    public function get(string $id): Profile
    {
        return $this->profileDao->getByUserId($id);
    }
}