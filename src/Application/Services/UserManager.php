<?php

declare(strict_types = 1);

namespace App\Application\Services;

use App\Domain\Model\User\Email;
use App\Domain\Model\User\UserName;
use App\Domain\Model\User\UserPassword;
use App\Persistence\Dao\UserDao;

final class UserManager
{
    private $userDao;

    public function __construct(UserDao $userDao)
    {
        $this->userDao = $userDao;
    }

    public function register(
        UserName $name,
        Email $email,
        UserPassword $password,
        UserPassword $passwordConfirmation
    ){
        if ( ! $password->equals($passwordConfirmation)) {
            throw new \InvalidArgumentException('Passwords do not match');
        }

        $userExists = $this->userDao->userExists($email);

        if ($userExists) {
            throw new \Exception("User already exists with email: $email");
        }

        $this->userDao->add($name, $email, $password);
        // TODO: Send Event
    }

}
