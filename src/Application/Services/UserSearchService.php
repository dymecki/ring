<?php

declare(strict_types = 1);

namespace App\Application\Services;

use App\Application\Services\Search\RequestParser\RequestParserFactory;
use App\Persistence\Dao\UserSearchDao;

final class UserSearchService
{
    private $repository;

    public function __construct()
    {
        $this->repository = new UserSearchDao();
    }

    public function search($tags): array
    {
        return $this->repository->search($tags);
    }

    public function factory($request): array
    {
        return RequestParserFactory::factory($request);
    }

    public function getUserFriends($userId = null)
    {
        return [];
    }

}
