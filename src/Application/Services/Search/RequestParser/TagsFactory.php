<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

final class TagsFactory
{
    private static $rules = [
        'gender'      => 'one',
        'tattoo'      => 'one',
        'zodiac'      => 'one',
        'alcohol'     => 'one',
        'language'    => 'many',
        'religion'    => 'one',
        'eye_color'   => 'one',
        'hair_color'  => 'one',
        'nationality' => 'one'
    ];

    public static function build($values): array
    {
        $result = [];

        foreach ($values as $key => $properties) {
            foreach ($properties as $property) {
                $result[] = strtolower($key . '_' . $property);
            }
        }

        return $result;
    }

    public static function tags($values): string
    {
        $result = [];

        foreach ($values as $key => $properties) {
            $result[] = self::text($key, $properties);
        }

        return '{' . implode(', ', array_filter($result)) . '}';
    }

    public static function stringTags($values): array
    {
//        var_dump(array_filter($values));exit;
        $values = array_filter($values);
        $arr    = [];

        foreach ($values as $key => $properties) {
            foreach ($properties as $index => $property) {
                if ( ! isset($arr[$key])) {
                    $arr[$key] = [];
                }

                $arr[$key][] .= strtolower($key . '_' . $index);
            }
        }

        foreach ($arr as &$property) {
            $property = '{' . implode(', ', $property) . '}';
        }

//        return array_map(function($property) {
//            return '{' . implode(', ', $property) . '}';
//        }, $arr);

        var_dump($arr);exit;

        return $arr;
    }

    private static function text($property, $values): string
    {
//        $result = [];
//
//        foreach ($values as $value) {
//            $result[] = $property . '_' . $value;
//        }
//
//        return implode(', ', $result);

        return implode(', ', array_map(function($item) use ($property) {
            return $property . '_' . $item;
        }, $values));
    }
}