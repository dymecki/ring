<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

final class NullRequestParser implements RequestParser
{
    public static function build($property)
    {
    }
}