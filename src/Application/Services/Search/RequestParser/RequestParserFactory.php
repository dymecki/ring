<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

final class RequestParserFactory
{
    public static function factory($request): array
    {
//        return array_map(function($name, $item) {
//            return self::getParser($name)::build($item);
//        }, array_keys($request), $request);


        $entities = [];
//        var_dump($request);exit;

        foreach ($request as $name => $item) {
            $entities[$name] = self::getParser($name)::build($item);

//            $className = self::getParser($name);
//            $entities[$name] = $className::build($item);
        }

        return $entities;
    }

    private static function getParser(string $formProperty): RequestParser
    {
        $namespace = 'App\\Application\\Services\\Search\\RequestParser\\';
        $property  = explode('_', strtolower($formProperty));
        $className = $namespace . implode('', array_map('ucfirst', $property)) . 'Parser';

        return class_exists($className) ? new $className : new NullRequestParser();
    }

}