<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

use App\Domain\Model\User\Properties\Gender;

final class GenderParser implements RequestParser
{
    public static function build($formGenders): array
    {
        $amount = \count($formGenders);

        if ( ! $amount || $amount > 2) {
            throw new \InvalidArgumentException(
                'There can be only 1 or 2 genders provided.'
            );
        }

        $genders[] = new Gender($formGenders[0]);

        if (isset($formGenders[1])) {
            $genders[] = new Gender($formGenders[1]);
        }

        return $genders;
    }

}