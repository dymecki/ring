<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

interface RequestParser
{
    public static function build($property);
}