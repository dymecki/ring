<?php

declare(strict_types = 1);

namespace App\Application\Services\Search\RequestParser;

use App\Domain\Model\User\Properties\Age;
use App\Domain\Model\User\Properties\AgeRange;

final class AgeParser implements RequestParser
{
    public static function build($property): array
    {
//        $isMin = isset($property['min']);
//        $isMax = isset($property['max']);
//
//        if ( ! $isMin && ! $isMax) {
//            throw new \InvalidArgumentException('Age property cannot be empty');
//        }
//
//        $ageMin = new Age($isMin ? (int) $property['min'] : Age::AGE_MIN);
//        $ageMax = new Age($isMax ? (int) $property['max'] : Age::AGE_MAX);
//
//        return array_flip(range($ageMin->value(), $ageMax->value()));

//        $ageRange = AgeRange::build((int) $property['min'], (int) $property['max']);

//        var_dump($ageRange->min());exit;

//        var_dump(array_flip($ageRange->range()));exit;
//        return array_flip($ageRange->range());

        return array_flip(
            AgeRange::build(
                (int) $property['min'],
                (int) $property['max']
            )->range()
        );
    }

}