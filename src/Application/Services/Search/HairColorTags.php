<?php

declare(strict_types = 1);

namespace App\Application\Services\Search;

use App\Domain\Model\User\Properties\HairColor;

final class HairColorTags
{
    private $values;

    public function __construct(HairColor $property)
    {
        $this->values = $property->allValues();
    }

    public function tags(): array
    {
        return array_map(function($item) {
            return strtolower('haircolor_' . $item);
        }, $this->values);
    }
}