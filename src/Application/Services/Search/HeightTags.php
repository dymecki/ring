<?php

declare(strict_types = 1);

namespace App\Application\Services\Search;

use App\Domain\Model\User\Properties\Height;

final class HeightTags
{
    private $values;

    public function __construct(Height $property)
    {
        $this->values = $property->allValues();
    }

    public function tags(): array
    {
        return array_map(function($item) {
            return strtolower('height_' . $item);
        }, $this->values);
    }

}