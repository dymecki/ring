<?php

declare(strict_types = 1);

namespace App\Application\Services\Search;

use App\Domain\Model\User\Properties\Age;

final class AgeTags
{
    private $values;

    public function __construct(Age $property)
    {
        $this->values = $property->allValidValues();
    }

    public function tags(): array
    {
        return array_map(function($item) {
            return strtolower('age_' . $item);
        }, $this->values);
    }

}