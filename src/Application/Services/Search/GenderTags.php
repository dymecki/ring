<?php

declare(strict_types = 1);

namespace App\Application\Services\Search;

use App\Domain\Model\User\Properties\Gender;

final class GenderTags
{
    private $values;

    public function __construct(Gender $property)
    {
        $this->values = $property->allValues();
    }

    public function tags(): array
    {
        return array_map(function($item) {
            return strtolower('gender_' . $item);
        }, $this->values);
    }

}