<?php

declare(strict_types = 1);

namespace App\Application\Services\Search;

use App\Domain\Model\User\Properties\HairColor;
use App\Domain\Model\User\Properties\Language;

final class LanguageTags
{
    private $values;

    public function __construct(Language $property)
    {
        $this->values = $property->allValues();
    }

    public function tags(): array
    {
        return array_map(function($item) {
            return strtolower('language_' . $item);
        }, $this->values);
    }

}