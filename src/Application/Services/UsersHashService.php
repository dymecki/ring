<?php

declare(strict_types = 1);

namespace App\Application\Services;

final class UsersHashService
{
    public static function hash($userA, $userB): string
    {
        return \md5($userA < $userB ? $userA . $userB : $userB . $userA);
    }
}