<?php

declare(strict_types = 1);

namespace App\Application\Services;

use App\Persistence\Dao\FriendsDao;

final class FriendsService
{
    private $friends;

    public function __construct()
    {
        $this->friends = new FriendsDao();
    }

    public function getUserFriends(string $user): array
    {
        $r = $this->friends->getUserFriends($user);
        //        var_dump($r);
        //        exit;
        return $r;
    }

    public function suggest($me, $friendId)
    {
        $hash = UsersHashService::hash($me, $friendId);

        $friends = $this->friends->checkIfFriends($hash);

        if ( ! $friends) {
            //            $this->friends->
        }
    }

    public function remove($user, $friend): void
    {
        $this->friends->remove($user, $friend);
    }

}
