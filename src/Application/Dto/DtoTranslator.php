<?php

declare(strict_types=1);

namespace App\Application\Dto;

final class DtoTranslator
{
    private $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    public function dto()
    {

    }

    private function mapper()
    {
        $mappings = [
            \App\Domain\Model\User\User::class => UserSearchResultDto::class
        ];
    }

}
