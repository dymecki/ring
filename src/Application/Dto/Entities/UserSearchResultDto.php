<?php

declare(strict_types=1);

namespace App\Application\Dto\Entities;

final class UserSearchResultDto
{
    private $name;
    private $age;
    private $imageUrl;
    private $gender;

    public function __construct(string $name, int $age, string $imageUrl, string $gender)
    {
        $this->name     = $name;
        $this->age      = $age;
        $this->imageUrl = $imageUrl;
        $this->gender   = $gender;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function __toString()
    {
        return json_encode([
            $this->name,
            $this->age,
            $this->imageUrl,
            $this->gender
        ]);
    }

}
