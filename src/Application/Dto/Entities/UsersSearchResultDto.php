<?php

declare(strict_types=1);

namespace App\Application\Dto\Entities;

final class UsersSearchResultDto
{
    private $users;

    public function __construct(array $users = [])
    {
        $this->users = $users;
    }

    public function add(UserSearchResultDto $user)
    {
        $this->users[] = $user;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function __toString()
    {
        return json_encode(['users' => $this->users]);
    }

}
