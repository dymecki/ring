<?php

declare(strict_types=1);

namespace App\Application;

final class PhotoSplit
{
    private $user;
    private $photoA;
    private $photoB;

    public function __construct($user, $photoA, $photoB)
    {
        $this->user   = $user;
        $this->photoA = $photoA;
        $this->photoB = $photoB;
    }

    public function save()
    {

    }

}
