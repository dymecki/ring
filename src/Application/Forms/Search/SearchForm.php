<?php

declare(strict_types = 1);

namespace App\Application\Forms\Search;

use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Form\Element\Checkbox;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\View\Renderer\PhpRenderer;

class SearchForm extends Form
{
    private $form;

    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add([
                       // This name will be used to fetch each checkbox from
                       // the CheckboxFieldset::class in the view template.
                       'name' => 'fieldsets',
                       'type' => MainFieldset::class
                   ]);

//        $this->add([
//                       'name'       => 'city',
//                       'attributes' => [
//                           'type'  => 'text',
//                           'placeholder'=>'Enter city name...',
//                           'class' => 'btn btn-primary'
//                       ],
//                   ]);

        $this->add([
                       'name'       => 'submit',
                       'attributes' => [
                           'type'  => 'submit',
                           'value' => 'Get Values',
                           'class' => 'btn btn-primary'
                       ],
                   ]);
    }

    public function build(): Form
    {
        $form = new Form('search');

        $gender = new Element\MultiCheckbox('gender');
        $gender->setLabel('Women');
        $gender->setValueOptions([
                                     'w' => 'Women',
                                     'm' => 'Men'
                                 ]);

        $city = new Element('city');
        $city->setLabel('Your city');
        $city->setAttributes([
                                 'type'        => 'text',
                                 'placeholder' => 'Enter city name...'
                             ]);

        $mainFieldset = new Fieldset('main');
        $mainFieldset->add($city);

        $submit = new Element('submit');
        $submit->setValue('Find');
        $submit->setAttributes(['type' => 'submit']);

        $form->add($mainFieldset);
        $form->add($gender);
        $form->add($city);
        $form->add($submit);

        return $form;
    }

}