<?php

declare(strict_types = 1);

namespace App\Application\Forms\Search;

use Zend\Form\Fieldset;
use Zend\Form\Element\Checkbox;

final class LookFieldset extends Fieldset
{
    public function init()
    {
        $this->add([
                       'name'       => 'name_1',
                       'type'       => Checkbox::class,
                       'options'    => [
                           'label'              => 'Value 1',
                           'use_hidden_element' => true,
                           'checked_value'      => 'yes',
                           'unchecked_value'    => 'no',
                       ],
                       'attributes' => [
                           'value' => 'no',
                       ],
                   ]);
    }
}