<?php

declare(strict_types = 1);

namespace App\Application\Forms\Search;

use Zend\Form\Fieldset;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Text;

final class MainFieldset extends Fieldset
{
    public function init()
    {
        $this->add([
                       'name'       => 'gender',
                       'type'       => Checkbox::class,
                       'options'    => [
                           'label'              => 'Women',
                           'use_hidden_element' => true,
                           'checked_value'      => 'w',
                           'unchecked_value'    => '',
                       ],
                       'attributes' => [
                           'value' => 'w',
                       ],
                   ]);

        $this->add([
                       'name'       => 'gender_2',
                       'type'       => Checkbox::class,
                       'options'    => [
                           'label'              => 'Men',
                           'use_hidden_element' => true,
                           'checked_value'      => 'm',
                           'unchecked_value'    => '',
                       ],
                       'attributes' => [
                           'value' => 'm',
                       ],
                   ]);

        $this->add([
                       'name'       => 'city',
                       'type'       => Text::class,
                       'options'    => [
                           'label'              => 'From',
                           'use_hidden_element' => true,
                           'unchecked_value'    => '',
                       ],
                       'attributes' => [
                           'placeholder' => 'Enter city name...',
                       ],
                   ]);
    }
}