<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Model\User\User;
use App\Domain\Model\User\Users;
use App\Domain\Model\User\Specification\Specification;
use App\Application\Dto\Entities\UserSearchResultDto;
use App\Application\Dto\Entities\UsersSearchResultDto;

final class SearchUsers
{
    private $specification;

    public function __construct(Specification $specification = null)
    {
        $this->specification = $specification;
    }

    public function get(): Users
    {
        $users = new Users();

        for ($i = 0; $i < 50; $i++) {
            $users->add(new User(
                new \App\Domain\Model\User\UserId(uniqid()),
                new \App\Domain\Model\User\UserName('Anna'),
                new \App\Domain\Model\User\Email('anna@yahoo.com')
            ));
        }

        return $users;
    }

    public function getDto()
    {
        $users = new UsersSearchResultDto();

        for ($i = 1; $i <= 50; $i++) {
            $users->add(new UserSearchResultDto(
                'Anna', 32, "$i.jpg", 'anna@yahoo.com'
            ));
        }

        return $users;
    }

}
