<?php

declare(strict_types = 1);

namespace App\Application;

use App\Domain\Model\User\UserId;
use App\Domain\Model\Conversation\Message;

final class SendInvitation
{
    private $messageService;

    public function __construct($messageService)
    {
        $this->messageService = $messageService;
    }

    public function send()
    {

    }

}
