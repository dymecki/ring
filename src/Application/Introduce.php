<?php

declare(strict_types = 1);

namespace App\Application;

use App\Domain\Model\User\UserId;

final class Introduce
{
    private $introducer;
    private $userA;
    private $userB;

    public function __construct($introducer, $userA, $userB)
    {
        $this->introducer = $introducer;
        $this->userA      = $userA;
        $this->userB      = $userB;
    }

    public function introduce()
    {

    }

}
