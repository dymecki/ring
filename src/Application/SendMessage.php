<?php

declare(strict_types = 1);

namespace App\Application;

use App\Domain\Model\User\UserId;
use App\Domain\Model\Conversation\Message;

final class SendMessage
{
    private $senderId;
    private $receiverId;
    private $message;

    function __construct(UserId $senderId, UserId $receiverId, Message $message)
    {
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->message    = $message;
    }

    public function send()
    {
        
    }

}
